var journey = require('journey');
var settings = require('./settings');

var journeyRouter = new (journey.Router);

journeyRouter.map(function () {

    this.root.bind(function (req, res) {
        res.send(200, {"Server":"HAL/1.0"}, "Welcome to: " + settings.applicationName);
        //res.sendBody("teste")
    });

    /**
     * Handle _GET
     */

        // just /database
    this.get('/databases').bind(function (req, res, id) {
        res.send('show me collections on: ' + id);
    });


    // databases/DATABASE_NAME
    this.get(/^databases\/([A-Za-z0-9_]+)$/).bind(function (req, res, id) {
        res.send('show me collections on: ' + id);
    });


    // databases/DATABASE_NAME/collections
    this.get(/^databases\/([A-Za-z0-9_]+)\/collections+$/).bind(function (req, res, dbid) {
        res.send('show me collections on: ' + dbid);
    });

    // databases/DATABASE_NAME/collections/COLLECTION_NAME
    this.get(/^databases\/([A-Za-z0-9_]+)\/collections+\/([A-Za-z0-9_]+)$/).bind(function (req, res, dbid, collid) {
        res.send('show me ' + collid + 'on:' + dbid);
    });

    // databases/DATABASE_NAME/collections/COLLECTION_NAME/documents
    this.get(/^databases\/([A-Za-z0-9_]+)\/collections+\/([A-Za-z0-9_]+)\/documents+$/).bind(function (req, res, dbid,
                                                                                                       collid) {
        res.send('show me all documents in ' + collid + 'on:' + dbid);
    });


    /**
     * _POST
     */

        // create database
    this.post('/databases').bind(function (req, res, data) {
        res.send(data);
    });


    //create a new collection
    this.post(/^databases\/([A-Za-z0-9_]+)\/collections+$/).bind(function (req, res, dbid, data) {
        res.send('create new collection on: ' + dbid);
    });

    // create s new document
    this.post(/^databases\/([A-Za-z0-9_]+)\/collections+\/([A-Za-z0-9_]+)\/documents+$/).bind(function (req, res, dbid,
                                                                                                        collid, data) {
        res.send('create new document in ' + collid + ' collection on:' + dbid);
    });

    /**
     * _PUT
     */

        //rename a collection
    this.put(/^databases\/([A-Za-z0-9_]+)\/collections+\/([A-Za-z0-9_]+)$/).bind(function (req, res, dbid, collid,
                                                                                           data) {
        res.send('reaname collection  ' + collid + ' on db: ' + dbid);
    });


    this.put(/^databases\/([A-Za-z0-9_]+)\/collections+\/([A-Za-z0-9_]+)\/documents+\/([A-Za-z0-9_]+)$/).bind(function (req,
                                                                                                                        res,
                                                                                                                        dbid,
                                                                                                                        collid,
                                                                                                                        docid,
                                                                                                                        data) {
        res.send('update ' + docid + 'in ' + collid + 'on: ' + dbid);
    });


    /**
     * _DELETE
     */

        //delete a database
    this.del(/^databases\/([A-Za-z0-9_]+)$/).bind(function (req, res, id) {
        res.send('delete: ' + id);
    });

    //delete a collection
    this.del(/^databases\/([A-Za-z0-9_]+)\/collections+\/([A-Za-z0-9_]+)$/).bind(function (req, res, dbid, collid) {
        res.send('delete collection ' + collid + ' on:' + dbid);
    });

    //delete a document
    this.del(/^databases\/([A-Za-z0-9_]+)\/collections+\/([A-Za-z0-9_]+)\/documents+\/([A-Za-z0-9_]+)$/).bind(function (req,
                                                                                                                        res,
                                                                                                                        dbid,
                                                                                                                        collid,
                                                                                                                        docid) {
        res.send('delete document ' + docid + 'in ' + collid + 'on: ' + dbid);
    });
});

// export router
var routers = {};
routers.journeyRouter = journeyRouter;
module.exports = routers;
