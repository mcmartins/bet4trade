var https = require('https');
var router = require("./router");
var settings = require('./settings');

https.createServer({key:settings.certificateKey, cert:settings.certificate},function (request, response) {

    /*if (request.client.authorized) {
     request.writeHead(200, {"Content-Type":"application/json"});
     request.end('{"status":"approved"}');
     } else {
     request.writeHead(401, {"Content-Type":"application/json"});
     request.end('{"status":"denied"}');
     }*/

    var body = "";

    request.addListener('data', function (chunk) { body += chunk });
    request.addListener('end', function () {

        router.journeyRouter.handle(request, body, function (result) {

            response.writeHead(result.status, result.headers);
            response.end(result.body);
        });
    });
}).listen(settings.serverPort, settings.serverHost);
