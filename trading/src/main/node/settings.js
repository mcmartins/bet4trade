var fs = require('fs');

var settings = {
    applicationName:"Bet4Trade | Trading Engine",
    serverPort:"8081",
    serverHost:"127.0.0.1",
    databasePort:7777,
    databaseConnectionString:'mysql://bet4trade:Z79gQn51w7Le@37.247.50.159:3306/bet4trade',
    certificateKey:fs.readFileSync('./certificate/privatekey.pem'),
    certificate:fs.readFileSync('./certificate/certificate.pem')
};

module.exports = settings;
