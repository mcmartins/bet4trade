var mysql = require('../node_modules/mysql');

// Create a connection object using mysql module
var connection = mysql.createConnection({
    host:'localhost',
    user:'me',
    password:'secret'
});

// Connect to database
// Throws err if exception occurs
var connect = function () {
    console.log('Connecting to database ' + connection.host + '...');
    connection.connect();
    connection.connect(function (err) {
        if (err) {
            console.log('Error connecting to the database: ', err);
            throw err;
        }
    });
};

// Close connection to database
// Throws err if exception occurs
var disconnect = function () {
    console.log('Disconnecting from database ' + connection.host + '...');
    connection.end(function (err) {
        if (err) {
            console.log('Error disconnecting to the database: ', err);
            throw err;
        }
    });
};

// Close connection to database
var forceDisconnect = function () {
    console.log('Disconnecting from database ' + connection.host + '...');
    connection.destroy();
};

// Execute a query
// Throws err if exception occurs
var execute = function (query) {

    connect();

    var result = null;

    console.log('Executing query: ' + query);
    connection.query(query, function (err, rows, fields) {
        if (err) {
            console.log('Error executing query: ', err);
            throw err;
        }

        result = rows;

        console.log('Query result: ', rows);
    });

    disconnect();

    return result;
};

// Export functions to use as node module
//exports.connect = connect;
//exports.disconnect = disconnect;
//exports.forceDisconnect = forceDisconnect;
//exports.execute = execute;


