var BetActive = connection.define("betActive", {
    "id":{ "type":"number" },
    "betId":{ "type":"number" },
    "asianLineId":{ "type":"number" },
    "marketId":{ "type":"number" },
    "selectionId":{ "type":"number" },
    "price":{ "type":"number" },
    "size":{ "type":"number" },
    "bspLiability":{ "type":"number" },
    "tradePrice":{ "type":"number" },
    "tradeSize":{ "type":"number" },
    "description":{ "type":"string" },
    "successTrade":{ "type":"string" },
    "betType":{ "type":"string" },
    "betCategoryType":{ "type":"string" },
    "betPersistenceType":{ "type":"string" },
    "matchedUnmatched":{ "type":"string" },
    "marketCloseDate":{ "type":"date" }
}, {
    "methods":{ "fullName":function () {
        return this.id + " " + this.betId;
    }}
});

var BetHistory = connection.define("betHistory", {
    "id":{ "type":"number" },
    "betId":{ "type":"number" },
    "asianLineId":{ "type":"number" },
    "marketId":{ "type":"number" },
    "selectionId":{ "type":"number" },
    "price":{ "type":"number" },
    "size":{ "type":"number" },
    "bspLiability":{ "type":"number" },
    "tradePrice":{ "type":"number" },
    "tradeSize":{ "type":"number" },
    "description":{ "type":"string" },
    "successTrade":{ "type":"string" },
    "betType":{ "type":"string" },
    "betCategoryType":{ "type":"string" },
    "betPersistenceType":{ "type":"string" },
    "matchedUnmatched":{ "type":"string" },
    "marketCloseDate":{ "type":"date" }
}, {
    "methods":{ "fullName":function () {
        return this.id + " " + this.betId;
    }}
});