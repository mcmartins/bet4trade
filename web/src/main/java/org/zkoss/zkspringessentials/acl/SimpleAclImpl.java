package org.zkoss.zkspringessentials.acl;

import java.util.List;

import org.springframework.security.acls.domain.AuditLogger;
import org.springframework.security.acls.domain.ConsoleAuditLogger;
import org.springframework.security.acls.model.AccessControlEntry;
import org.springframework.security.acls.model.Acl;
import org.springframework.security.acls.model.NotFoundException;
import org.springframework.security.acls.model.ObjectIdentity;
import org.springframework.security.acls.model.Permission;
import org.springframework.security.acls.model.Sid;

/**
 * Very simple implementation of Acl interface, based on org.springframework.security.acls.domain.AclImpl source (mainly
 * the isGranted method code). This implementation neither use owner concept nor parent concept.
 */
public class SimpleAclImpl implements Acl {

    private static final long serialVersionUID = 1L;
    private ObjectIdentity oi;
    private List<AccessControlEntry> aces;
    private transient AuditLogger auditLogger = new ConsoleAuditLogger();

    public SimpleAclImpl() {
    }

    @Override
    public List<AccessControlEntry> getEntries() {
        return this.aces;
    }

    @Override
    public ObjectIdentity getObjectIdentity() {
        return this.oi;
    }

    @Override
    public Sid getOwner() {
        return null; // owner concept is optional, we don't use it
    }

    @Override
    public Acl getParentAcl() {
        return null; // we don't use inheritance
    }

    @Override
    public boolean isEntriesInheriting() {
        return false; // we don't use inheritance
    }

    @Override
    public boolean isGranted(final List<Permission> permission, final List<Sid> sids,
            final boolean administrativeMode) throws NotFoundException {

        AccessControlEntry firstRejection = null;

        for (final Permission aPermission : permission) {
            for (final Sid sid : sids) {
                // Attempt to find exact match for this permission mask and SID
                boolean scanNextSid = true;

                for (final AccessControlEntry ace1 : this.aces) {

                    if ((ace1.getPermission().getMask() == aPermission.getMask()) && ace1.getSid().equals(sid)) {
                        // Found a matching ACE, so its authorization decision
                        // will prevail
                        if (ace1.isGranting()) {
                            // Success
                            if (!administrativeMode) {
                                this.auditLogger.logIfNeeded(true, ace1);
                            }

                            return true;
                        } else {
                            // Failure for this permission, so stop search
                            // We will see if they have a different permission
                            // (this permission is 100% rejected for this SID)
                            if (firstRejection == null) {
                                // Store first rejection for auditing reasons
                                firstRejection = ace1;
                            }

                            scanNextSid = false; // helps break the loop

                            break; // exit "aces" loop
                        }
                    }
                }

                if (!scanNextSid) {
                    break; // exit SID for loop (now try next permission)
                }
            }
        }

        if (firstRejection != null) {
            // We found an ACE to reject the request at this point, as no
            // other ACEs were found that granted a different permission
            if (!administrativeMode) {
                this.auditLogger.logIfNeeded(false, firstRejection);
            }

            return false;
        }

        throw new NotFoundException("Unable to locate a matching ACE for passed permissions and SIDs");
    }

    @Override
    public boolean isSidLoaded(final List<Sid> sids) {
        // we use in-memory structure, not external DB, so all entries are
        // always loaded
        // (if I correctly understand meaning of this method)
        return true;
    }
}
