/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.web.filter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.zip.GZIPOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

/**
 * <description>.
 *
 * @author Manuel Martins
 * @version $Revision: 6259 $
 */
public class CompressionGzipResponseWrapper extends HttpServletResponseWrapper {

    public static final String CONTENT_ENCODING_HEADER = "Content-Encoding";
    public static final String CONTENT_LENGTH_HEADER = "Content-Length";
    public static final String ENCODING = "UTF-8";

    private HttpServletResponse origResponse = null;
    private ServletOutputStream servletOutputStream = null;
    private PrintWriter printWriter = null;

    public CompressionGzipResponseWrapper(HttpServletResponse response) {
        super(response);
        this.origResponse = response;
    }

    public ServletOutputStream createOutputStream() throws IOException {
        return (new GZIPResponseStream(this.origResponse));
    }

    public void finishResponse() {
        try {
            if (this.printWriter != null) {
                this.printWriter.close();
            } else {
                if (this.servletOutputStream != null) {
                    this.servletOutputStream.close();
                }
            }
        } catch (final IOException ignore) {
        }
    }

    @Override
    public void flushBuffer() throws IOException {
        if (this.servletOutputStream != null) {
            this.servletOutputStream.flush();
        }
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException {
        if (this.printWriter != null) {
            throw new IllegalStateException("getWriter() has already been called!");
        }

        if (this.servletOutputStream == null) {
            this.servletOutputStream = createOutputStream();
        }

        return (this.servletOutputStream);
    }

    @Override
    public PrintWriter getWriter() throws IOException {
        if (this.printWriter != null) {
            return (printWriter);
        }

        if (this.servletOutputStream != null) {
            throw new IllegalStateException("getOutputStream() has already been called!");
        }

        this.servletOutputStream = createOutputStream();
        this.printWriter = new PrintWriter(new OutputStreamWriter(this.servletOutputStream, ENCODING));

        return this.printWriter;
    }

    @Override
    public void setContentLength(int length) {
    }

    /**
     * <description>.
     *
     * @author Manuel Martins
     * @version $Revision: 6259 $
     */
    class GZIPResponseStream extends ServletOutputStream {

        private ByteArrayOutputStream byteArrayOutputStream = null;
        private GZIPOutputStream gzipOutputStream = null;
        private boolean closed = false;
        private HttpServletResponse httpServletResponse = null;
        private ServletOutputStream servletOutputStream = null;

        public GZIPResponseStream(HttpServletResponse httpServletResponse) throws IOException {
            this.closed = false;
            this.httpServletResponse = httpServletResponse;
            this.servletOutputStream = httpServletResponse.getOutputStream();
            this.byteArrayOutputStream = new ByteArrayOutputStream();
            this.gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream);
        }

        @Override
        public void close() throws IOException {
            if (this.closed) {
                throw new IOException("This output servletOutputStream has already been closed");
            }

            this.gzipOutputStream.finish();

            final byte[] bytes = this.byteArrayOutputStream.toByteArray();

            this.httpServletResponse.addHeader(CONTENT_LENGTH_HEADER, Integer.toString(bytes.length));
            this.httpServletResponse.addHeader(CONTENT_ENCODING_HEADER, CompressionFilter.GZIP);
            this.servletOutputStream.write(bytes);
            this.servletOutputStream.flush();
            this.servletOutputStream.close();
            this.closed = true;
        }

        @Override
        public void flush() throws IOException {
            if (this.closed) {
                throw new IOException("Cannot flush a closed output servletOutputStream");
            }
            this.gzipOutputStream.flush();
        }

        @Override
        public void write(int b) throws IOException {
            if (this.closed) {
                throw new IOException("Cannot write to a closed output servletOutputStream");
            }
            this.gzipOutputStream.write((byte) b);
        }

        @Override
        public void write(byte b[]) throws IOException {
            write(b, 0, b.length);
        }

        @Override
        public void write(byte b[], int off, int len) throws IOException {
            if (this.closed) {
                throw new IOException("Cannot write to a closed output servletOutputStream");
            }
            this.gzipOutputStream.write(b, off, len);
        }
    }
}
