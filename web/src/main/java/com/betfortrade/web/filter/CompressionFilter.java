/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Compresses the response to gzip.
 *
 * @author Manuel Martins
 * @version $Revision: 6259 $
 */
public class CompressionFilter implements Filter {

    public static final String GZIP = "gzip";
    public static final String ACCEPT_ENCODING = "accept-encoding";

    @Override
    public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse,
            final FilterChain filterChain) throws IOException, ServletException {
        if (servletRequest instanceof HttpServletRequest) {
            final HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
            final HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
            final String httpServletRequestHeader = httpServletRequest.getHeader(ACCEPT_ENCODING);
            if (httpServletRequestHeader != null && httpServletRequestHeader.contains(GZIP)) {
                final CompressionGzipResponseWrapper compressionGzipResponseWrapper =
                        new CompressionGzipResponseWrapper(httpServletResponse);
                filterChain.doFilter(servletRequest, compressionGzipResponseWrapper);
                compressionGzipResponseWrapper.finishResponse();
                return;
            }
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void init(final FilterConfig filterConfig) {

    }

    @Override
    public void destroy() {

    }
}
