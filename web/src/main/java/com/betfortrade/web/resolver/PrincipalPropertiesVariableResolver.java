/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.web.resolver;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import org.zkoss.xel.VariableResolver;
import org.zkoss.xel.XelException;

import com.betfortrade.core.authentication.PrincipalProperties;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public class PrincipalPropertiesVariableResolver implements VariableResolver, Serializable {

    @Override
    public Object resolveVariable(final String name) throws XelException {
        PrincipalProperties principalProperties = null;
        final List<PrincipalProperties> principalPropertiesList = Arrays.asList(PrincipalProperties.values());
        for (final PrincipalProperties principalProp : principalPropertiesList) {
            if (principalProp.name().equalsIgnoreCase(name)) {
                principalProperties = principalProp;
                break;
            }
        }
        return principalProperties;
    }
}
