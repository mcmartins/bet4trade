/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.web.resolver;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import org.zkoss.xel.VariableResolver;
import org.zkoss.xel.XelException;

import com.betfortrade.core.util.ApplicationURL;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class ApplicationURLVariableResolver implements VariableResolver, Serializable {

    @Override
    public Object resolveVariable(final String name) throws XelException {
        ApplicationURL url = null;
        final List<ApplicationURL> applicationURLs = Arrays.asList(ApplicationURL.values());
        for (final ApplicationURL aURL : applicationURLs) {
            if (aURL.name().equalsIgnoreCase(name)) {
                url = aURL;
                break;
            }
        }
        return url;
    }
}
