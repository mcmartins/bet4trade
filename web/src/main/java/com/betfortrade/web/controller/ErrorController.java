/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.web.controller;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zul.Window;

import com.betfortrade.core.bean.BeanUtil;
import com.betfortrade.core.util.ApplicationContainerUtils;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class ErrorController extends SelectorComposer<Window> {

    @Override
    public void doAfterCompose(final Window comp) throws Exception {
        comp.detach();
    }

    @Override
    public ComponentInfo doBeforeCompose(final Page page, final Component parent, final ComponentInfo compInfo) {
        final Throwable throwable = (Throwable) Executions.getCurrent().getAttribute("javax.servlet.error.exception");
        final ApplicationContainerUtils applicationContainerUtils = BeanUtil.getBean(ApplicationContainerUtils.class);
        applicationContainerUtils.showErrorMessage(throwable.getMessage());
        return compInfo;
    }
}
