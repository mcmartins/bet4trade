/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.web.controller;

import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Panelchildren;

import com.betfortrade.core.bean.BeanUtil;
import com.betfortrade.core.util.ApplicationContainerUtils;
import com.betfortrade.core.util.ApplicationURL;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class PageContainerController extends SelectorComposer<Panelchildren> {

    @Override
    public void doAfterCompose(final Panelchildren center) {
        //new desktop was created,
        //lets set the new application page container on the application container utility singleton
        final ApplicationContainerUtils applicationContainerUtils = BeanUtil.getBean(ApplicationContainerUtils.class);
        applicationContainerUtils.setPageContainer(center);
        //TODO remove after finish
        applicationContainerUtils.openPage(ApplicationURL.APPLICATION_COMING_SOON.getZul());
    }
}
