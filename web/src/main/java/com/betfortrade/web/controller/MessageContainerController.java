/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.web.controller;

import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zul.Vlayout;

import com.betfortrade.core.bean.BeanUtil;
import com.betfortrade.core.util.ApplicationContainerUtils;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class MessageContainerController extends SelectorComposer<Vlayout> {

    @Override
    public void doAfterCompose(final Vlayout vlayout) {
        //new desktop was created,
        //lets set the new application message container on the application container utility singleton
        final ApplicationContainerUtils applicationContainerUtils = BeanUtil.getBean(ApplicationContainerUtils.class);
        applicationContainerUtils.setMessagesContainer(vlayout);
    }
}
