/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.web.controller;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zul.Div;

import com.betfortrade.core.bean.BeanUtil;
import com.betfortrade.core.util.ApplicationContainerUtils;
import com.betfortrade.core.util.ApplicationURL;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public class IndexController extends SelectorComposer<Div> {

    @Override
    public void doAfterCompose(final Div div) {
        //new desktop was created,
        //lets set the new application desktop container on the application container utility singleton
        final ApplicationContainerUtils applicationContainerUtils = BeanUtil.getBean(ApplicationContainerUtils.class);
        applicationContainerUtils.setDesktopMainContainer(div);
        //what we are going to do?
        final String action = Executions.getCurrent().getParameter(ApplicationURL.ACTION);
        if (action != null) {
            //nice, is there any application url?
            final ApplicationURL applicationURLTranslated = ApplicationURL.resolveApplicationURLByAction(action);
            if (applicationURLTranslated != null) {
                //ok, is it to open in which mode?
                if (applicationURLTranslated.getMode().equals(ApplicationURL.OpenMode.MODAL)) {
                    //modal mode, where a pop up is above the desktop
                    applicationContainerUtils.openPageModal(applicationURLTranslated.getZul(),
                            applicationURLTranslated.getAction());
                } else if (applicationURLTranslated.getMode().equals(ApplicationURL.OpenMode.PAGE)) {
                    //page mode, where the page is loaded in the reserved area for pages
                    applicationContainerUtils.openPage(applicationURLTranslated.getZul());
                } else if (applicationURLTranslated.getMode().equals(ApplicationURL.OpenMode.DESKTOP)) {
                    //desktop mode, reload everything
                    Executions.sendRedirect("/");
                }
            } else {
                applicationContainerUtils.openPage(ApplicationURL.APPLICATION_NO_ACTION.getZul());
            }
        }
    }
}
