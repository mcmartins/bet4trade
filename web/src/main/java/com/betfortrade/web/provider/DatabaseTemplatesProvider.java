/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.web.provider;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.Principal;
import java.util.Locale;

import org.apache.commons.collections.ExtendedProperties;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.resource.Resource;
import org.apache.velocity.runtime.resource.loader.ResourceLoader;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zk.ui.Executions;

import com.betfortrade.data.api.service.general.TemplateService;
import com.betfortrade.data.api.service.user.UserService;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class DatabaseTemplatesProvider extends ResourceLoader {

    @Autowired
    private TemplateService templateService;

    @Autowired
    private UserService userService;

    @Override
    public void init(ExtendedProperties configuration) {
        //nothing to do...will use the dao to access templates...
    }

    @Override
    public boolean isSourceModified(final Resource resource) {
        return resource.getLastModified() != readLastModified(resource);
    }

    @Override
    public long getLastModified(final Resource resource) {
        return readLastModified(resource);
    }

    @Override
    public synchronized InputStream getResourceStream(final String name)
            throws ResourceNotFoundException {
        if (StringUtils.isBlank(name)) {
            throw new ResourceNotFoundException("DataSourceResourceLoader: Template name was empty or null");
        }

        final Principal userPrincipal =
                Executions.getCurrent() != null ? Executions.getCurrent().getUserPrincipal() : null;
        Locale locale = Locale.UK;
        if (userPrincipal != null) {
            locale = this.userService.getUserLanguage(userPrincipal.getName());
        }
        final String template = this.templateService.getTemplate(name, locale);
        return new ByteArrayInputStream(
                StringUtils.isNotBlank(template) ? template.getBytes() : "NO TEMPLATE FOUND".getBytes());
    }

    /**
     * Fetches the last modification time of the resource
     *
     * @param resource Resource object we are finding timestamp of
     * @return timestamp as long
     */
    private long readLastModified(final Resource resource) {
        final String name = resource.getName();
        if (StringUtils.isBlank(name)) {
            throw new ResourceNotFoundException("DataSourceResourceLoader: Template name was empty or null");
        }

        DateTime dateTime = this.templateService.getTemplateLastModifiedDate(name);
        if (dateTime == null) {
            throw new ResourceNotFoundException("DataSourceResourceLoader: Template does not exist");
        }

        return dateTime.getMillis();
    }
}
