/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.web.provider;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.betfortrade.core.authentication.ApplicationPrincipal;
import com.betfortrade.data.api.entities.user.Role;
import com.betfortrade.data.api.entities.user.User;
import com.betfortrade.data.api.service.user.UserService;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class DatabaseAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService userService;

    @Override
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {
        Authentication authenticationToken;
        //lets check if the user exists on the database
        final User user = this.userService.getUserByEmail(authentication.getName());
        if (user != null) {
            //check if subscription is valid
            if (this.userService.isSubscriptionValid(authentication.getName())) {
                //nice, now if the user has a temporary password we should check that one
                final String password = user.getPassword();
                if (this.passwordEncoder.matches((String) authentication.getCredentials(), password)) {
                    //lets get the user roles
                    final List<GrantedAuthority> AUTHORITIES = new ArrayList<GrantedAuthority>();
                    for (final Role role : user.getRoles()) {
                        AUTHORITIES.add(new SimpleGrantedAuthority(role.getName()));
                    }
                    //ok, we can create the authentication token
                    final ApplicationPrincipal applicationPrincipal =
                            new ApplicationPrincipal(user.getPerson().getPreferredName(), user.getEmail());
                    authenticationToken =
                            new UsernamePasswordAuthenticationToken(applicationPrincipal, authentication.getCredentials(), AUTHORITIES);
                } else {
                    throw new BadCredentialsException("OOPS! The credentials you provided are wrong! Please try again...");
                }
            } else {
                throw new CredentialsExpiredException("OOPS! The subscription is not valid! Please contact the support team: support@bet4trade.com.");
            }
        } else {
            throw new UsernameNotFoundException("OOPS! The user does not exist! What are you trying to do!?");
        }
        //if the token is null is because something is wrong with the credentials...perhaps the password is invalid
        return authenticationToken;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
