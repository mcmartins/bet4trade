<?page id="users" title="inbox" cacheable="false"?>
<?meta content="text/html; charset=UTF-8" pageEncoding="UTF-8"?>
<!--<?taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" ?>-->

<zk xmlns="http://www.zkoss.org/2005/zul"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.zkoss.org/2005/zul http://www.zkoss.org/2005/zul/zul.xsd"
    xmlns:h="http://www.w3.org/1999/xhtml">

  <window if="${sec:isAllGranted('ROLE_ADMINISTRATOR')}"
          border="none" apply="com.betfortrade.ui.bind.ApplicationBindComposer"
          viewModel="@id('vm') @init('com.betfortrade.ui.admin.AdminUsersEditViewModel')">

    <listbox vflex="true" hflex="1" model="@load(vm.users)" selectedItem="@bind(vm.selected)">
      <auxhead>
        <auxheader colspan="3">Users List</auxheader>
      </auxhead>
      <listhead>
        <listheader label="First Name" width="80px" align="center"/>
        <listheader label="Last Name" align="center"/>
        <listheader label="Username" width="80px" align="center"/>
      </listhead>
      <template name="model" var="user">
        <listitem>
          <listcell label="@load(user.person.firstName)"/>
          <listcell label="@load(user.person.lastName)"/>
          <listcell label="@load(user.email)"/>
        </listitem>
      </template>
    </listbox>

    <hbox>
      <div>
        <div sclass="control-group">
          <h:label class="control-label">First Name</h:label>
          <div sclass="controls">
            <textbox type="text" id="firstName" name="firstName"
                     value="@bind(vm.selected.person.firstName)"
                     onChange="preferredName.value=self.value"
                     maxlength="255"/>
          </div>
        </div>

        <div sclass="control-group">
          <h:label class="control-label">Last Name</h:label>
          <div sclass="controls">
            <textbox type="text" id="lastName"
                     value="@bind(vm.selected.person.lastName)"
                     name="lastName" maxlength="255"/>
          </div>
        </div>

        <div sclass="control-group">
          <h:label class="control-label">Preferred Name</h:label>
          <div sclass="controls">
            <textbox type="text" id="preferredName"
                     value="@bind(vm.selected.person.preferredName)"
                     name="preferredName" maxlength="100"/>
          </div>
        </div>

        <div sclass="control-group">
          <h:label class="control-label">Phone</h:label>
          <div sclass="controls">
            <textbox id="phone" name="phone"
                     value="@bind(vm.selected.person.phoneNumber)"
                     maxlength="25" sclass="medium-large"/>
          </div>
        </div>

        <div sclass="control-group">
          <h:label class="control-label">Birth Date</h:label>
          <div sclass="controls">
            <datebox id="birthday" name="Birthdate"
                     value="@bind(vm.selected.person.birthDate.toDate)"
                     format="yyyy/MM/dd" sclass="medium"/>
          </div>
        </div>

        <div sclass="control-group">
          <h:label class="control-label">Address 1</h:label>
          <div sclass="controls">
            <textbox type="text" id="address1"
                     value="@bind(vm.selected.person.address1)"
                     name="address1" maxlength="255"/>
          </div>
        </div>

        <div sclass="control-group">
          <h:label class="control-label">Address 2</h:label>
          <div sclass="controls">
            <textbox type="text" id="address2"
                     value="@bind(vm.selected.person.address2)"
                     name="address2" maxlength="255"/>
          </div>
        </div>

        <div sclass="control-group">
          <h:label class="control-label">City</h:label>
          <div sclass="controls">
            <textbox type="text" id="city" name="city"
                     value="@bind(vm.selected.person.city)"
                     maxlength="255" sclass="medium-large"/>
          </div>
        </div>

        <div sclass="control-group">
          <h:label class="control-label">Zip Code</h:label>
          <div sclass="controls">
            <textbox type="text" id="zipCode"
                     value="@bind(vm.selected.person.postalCode)"
                     name="zipCode" maxlength="10" sclass="medium"/>
          </div>
        </div>

        <div sclass="control-group">
          <h:label class="control-label">Country</h:label>
          <div sclass="controls" apply="org.zkoss.bind.BindComposer"
               viewModel="@id('vm') @init('com.betfortrade.ui.common.CountryViewModel')">
            <combobox id="country" model="@load(vm.countries)"
                      value="@bind(vm.selected.person.country)"
                      readonly="true">
              <template name="model" var="each">
                <comboitem label="@load(each.prettyName)" value="@load(each.countryPK.countryISO)"/>
              </template>
            </combobox>
          </div>
        </div>
      </div>
      <div>
        <div sclass="control-group">
          <h:label class="control-label">E-mail</h:label>
          <div sclass="controls">
            <textbox type="text" id="email" name="email"
                     value="@bind(vm.selected.email)"
                     constraint="/.+@.+\.[a-z]+/: Please enter a valid email."
                     onChange="username.value=self.value" maxlength="255" sclass="xtra-large"/>
          </div>
        </div>

        <div sclass="control-group">
          <h:label class="control-label">Username</h:label>
          <div sclass="controls">
            <textbox type="text" id="username"
                     value="@bind(vm.selected.username)"
                     name="username" readonly="true" inplace="true" maxlength="100"/>
          </div>
        </div>

        <div sclass="control-group">
          <h:label class="control-label">Language</h:label>
          <div sclass="controls" apply="org.zkoss.bind.BindComposer"
               viewModel="@id('vm') @init('com.betfortrade.ui.common.LanguageViewModel')">
            <combobox id="language" model="@load(vm.languages)"
                      value="@bind(vm.selected.profile.language)"
                      readonly="true">
              <template name="model" var="each">
                <comboitem label="@load(each.prettyName)" value="@load(each.id.language)"/>
              </template>
            </combobox>
          </div>
        </div>
      </div>
    </hbox>
  </window>

</zk>
