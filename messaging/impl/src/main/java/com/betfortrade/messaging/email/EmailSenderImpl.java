/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.messaging.email;

import java.text.MessageFormat;
import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.betfortrade.core.common.Name;
import com.betfortrade.messaging.api.email.EmailSender;
import com.google.common.base.Charsets;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public class EmailSenderImpl implements EmailSender<SimpleMailMessage> {

    private static final Logger LOG = Logger.getLogger(EmailSenderImpl.class);

    private final VelocityEngine velocityEngine;
    private final JavaMailSender mailSender;

    /**
     * Constructor
     */
    @Autowired
    public EmailSenderImpl(final VelocityEngine velocityEngine, final JavaMailSender mailSender) {
        this.velocityEngine = velocityEngine;
        this.mailSender = mailSender;
    }

    @Override
    public void send(final SimpleMailMessage mailMessage, final Name<String> template,
            final Map<String, Object> templateVariables) {
        final MimeMessagePreparator mimeMessagePreparator = new MimeMessagePreparator() {
            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                final MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
                message.setTo(mailMessage.getTo());
                message.setFrom(mailMessage.getFrom());
                message.setSubject(mailMessage.getSubject());

                final String body = VelocityEngineUtils.mergeTemplateIntoString(EmailSenderImpl.this.velocityEngine,
                        template.getName(), Charsets.UTF_8.displayName(), templateVariables);

                mailMessage.setText(body);

                message.setText(body, true);
            }
        };


        LOG.info(MessageFormat.format("Sending e-mail to ''{0}''.", mailMessage.getTo()));

        if (LOG.isDebugEnabled()) {
            LOG.debug(MessageFormat.format("subject = ''{0}''", mailMessage.getSubject()));
            LOG.debug(MessageFormat.format("body = ''{0}''", mailMessage.getText()));
        }

        this.mailSender.send(mimeMessagePreparator);
    }

}
