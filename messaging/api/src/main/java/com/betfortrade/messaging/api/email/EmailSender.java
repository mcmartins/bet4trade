/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.messaging.api.email;

import com.betfortrade.messaging.api.MessagingTemplateSender;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public interface EmailSender<T> extends MessagingTemplateSender<T> {
}
