/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.messaging.api.template;

import com.betfortrade.core.common.Name;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public enum Templates implements Name<String> {

    RECOVERY_PASSWORD("recovery-password.vm"),
    NOTIFICATION("notification.vm"),
    REGISTRATION("registration.vm"),
    SUBSCRIPTION_EXPIRED("subscription-expired.vm"),
    SUBSCRIPTION_EXPIRING("subscription-expiring.vm"),
    SUBSCRIPTION_RENEWAL("subscription-renewal.vm"),
    NEWSLETTER_SUBSCRIPTION("newsletter-subscription.vm");

    private String name;

    private Templates(final String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

}
