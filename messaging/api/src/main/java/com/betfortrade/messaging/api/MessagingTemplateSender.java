/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.messaging.api;

import java.util.Map;

import com.betfortrade.core.common.Name;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public interface MessagingTemplateSender<T> {

    /**
     * Sends a message using Velocity template for the body and the properties passed in as Velocity variables.
     *
     * @param message The message to be sent, except for the body.
     * @param template the Velocity template name.
     * @param templateVariables Variables to use when processing the template.
     */
    void send(T message, Name<String> template, Map<String, Object> templateVariables);
}
