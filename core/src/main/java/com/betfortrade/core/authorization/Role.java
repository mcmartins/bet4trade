/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.authorization;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public enum Role {

    ROLE_ADMINISTRATOR, ROLE_SUPER_USER, ROLE_USER
}
