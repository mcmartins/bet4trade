/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.resource;

import java.text.MessageFormat;
import java.util.Locale;

import com.betfortrade.core.common.Key;
import com.google.common.base.Preconditions;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
final class ResourceHandlerKeyImpl implements Key<String> {

    private final String packageName;
    private final Locale locale;

    ResourceHandlerKeyImpl(final String packageName, final Locale locale) {
        this.packageName = Preconditions.checkNotNull(packageName);
        this.locale = locale == null ? Locale.getDefault() : locale;
    }

    public static Key getInstance(final String packageName, final Locale locale) {
        return new ResourceHandlerKeyImpl(packageName, locale);
    }

    @Override
    public String getKey() {
        return MessageFormat.format("[{0}_{1}]", this.packageName, this.locale);
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof Key) {
            return this.getKey().equals(((Key) obj).getKey());
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getKey() != null ? this.getKey().hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return this.getKey();
    }
}
