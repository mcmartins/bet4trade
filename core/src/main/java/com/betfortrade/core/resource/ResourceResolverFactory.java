/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.resource;

import java.util.Locale;

import com.betfortrade.core.resource.api.ResourceResolver;
import com.betfortrade.core.resource.api.ResourceResolverWrapper;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public enum ResourceResolverFactory {

    INSTANCE(new ResourceResolverWrapperImpl());

    private ResourceResolverWrapper<?> resourceResolver;

    ResourceResolverFactory(final ResourceResolverWrapper<?> resourceResolver) {
        this.resourceResolver = resourceResolver;
    }

    public ResourceResolver<?> getResourceResolver(final Class<?> clazz, final Locale locale) {
        return this.resourceResolver.getResourceResolver(clazz, locale);
    }
}
