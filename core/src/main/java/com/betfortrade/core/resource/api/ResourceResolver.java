/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.resource.api;

import java.util.Locale;

import com.betfortrade.core.common.Code;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public interface ResourceResolver<T> {

    String resolve(Code<T> resourceCode, Object... parameters);

    String resolve(Code<T> resourceCode, Locale locale, Object... parameters);
}
