/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.resource.api;

import java.util.Locale;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public interface ResourceResolverWrapper<T> {

    ResourceResolver<T> getResourceResolver(Class<?> clazz, Locale locale);
}
