/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.resource;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.betfortrade.core.common.Code;
import com.betfortrade.core.common.Key;
import com.betfortrade.core.resource.api.ResourceResolver;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
class ResourceResolverImpl implements ResourceResolver<String> {

    private static final Logger LOG = Logger.getLogger(ResourceResolverImpl.class);

    private static final String RESOURCES_PACKAGE = "resources";

    private static final Map<Key, ResourceResolverImpl> resourceResolvers = new HashMap<Key, ResourceResolverImpl>();

    private ResourceBundle resources;

    ResourceResolverImpl() {
    }

    @Override
    public String resolve(final Code<String> resourceCode, final Object... parameters) {
        return this.resolve(resourceCode, null, parameters);
    }

    @Override
    public String resolve(final Code<String> resourceCode, final Locale locale, final Object... parameters) {
        String message = resourceCode.getCode();

        if (this.resources != null) {
            try {
                message = this.resources.getString(resourceCode.getCode());
            } catch (final MissingResourceException e) {
                LOG.error(e);
                message = e.getMessage();
            }

            if (message != null) {
                if (parameters != null) {
                    // Replaces parameters
                    message = MessageFormat.format(message, parameters);
                }
            }
        }
        return message;
    }

    public synchronized ResourceResolver<String> getResourceResolver(final Class<?> resourceClass, Locale locale) {
        final String packageName = resourceClass.getPackage().getName();
        final Key key = ResourceHandlerKeyImpl.getInstance(packageName, locale);
        ResourceResolverImpl handler = resourceResolvers.get(key);
        if (handler == null) {
            try {
                locale = locale == null ? Locale.getDefault() : locale;
                // Load resources from file <packageName>.resources.resources[<locale>].properties using resources class loader.
                this.resources = ResourceBundle.getBundle(packageName + RESOURCES_PACKAGE,
                        locale, resourceClass.getClassLoader());
            } catch (final MissingResourceException e) {
                LOG.error(e);
            }
            handler = this;
            resourceResolvers.put(key, handler);
        }
        return handler;
    }
}
