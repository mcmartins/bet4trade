/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.resource;

import java.util.Locale;

import com.betfortrade.core.resource.api.ResourceResolver;
import com.betfortrade.core.resource.api.ResourceResolverWrapper;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
class ResourceResolverWrapperImpl implements ResourceResolverWrapper<String> {

    private final ResourceResolver<String> resourceResolver;

    ResourceResolverWrapperImpl() {
        this.resourceResolver = new ResourceResolverImpl();
    }

    @Override
    public ResourceResolver<String> getResourceResolver(final Class<?> clazz, final Locale locale) {
        return ((ResourceResolverImpl) resourceResolver).getResourceResolver(clazz, locale);
    }
}
