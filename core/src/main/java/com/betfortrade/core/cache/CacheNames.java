/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.cache;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class CacheNames {

    public static final String COUNTRY = "country";
    public static final String LANGUAGE = "language";
    public static final String TEMPLATE = "template";

}
