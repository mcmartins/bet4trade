/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.authentication;

import java.security.Principal;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public final class ApplicationPrincipal implements Principal {

    private String preferredName;
    private String email;

    public ApplicationPrincipal(final String preferredName, final String email) {
        this.preferredName = preferredName;
        this.email = email;
    }

    public String getEmail() {
        return this.email;
    }

    public String getPreferredName() {
        return this.preferredName;
    }

    @Override
    public String getName() {
        return this.email;
    }

    @Override
    public String toString() {
        return this.getEmail();
    }
}
