/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.authentication;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import com.betfortrade.core.bean.BeanUtil;
import com.betfortrade.core.util.StringUtils;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public enum PrincipalProperties {

    PRINCIPAL_PREFERRED_NAME {
        @Override
        public String getValue() {
            return getApplicationPrincipal().getPreferredName();
        }
    },
    PRINCIPAL_EMAIL {
        @Override
        public String getValue() {
            return getApplicationPrincipal().getEmail();
        }
    };

    public static ApplicationPrincipal getApplicationPrincipal() {
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ApplicationPrincipal user = null;
        if (auth != null) {
            if (auth.getPrincipal() instanceof ApplicationPrincipal) {
                user = (ApplicationPrincipal) auth.getPrincipal();
            } else if (auth.getPrincipal() instanceof User) {
                final User userDetails = (User) auth.getPrincipal();
                // In this case we can not get the user preferred name :(
                user = new ApplicationPrincipal(userDetails.getUsername(), userDetails.getUsername());
            }
        }
        if (user == null) {
            user = new ApplicationPrincipal("", "");
        }
        return user;
    }

    public String getValue() {
        return StringUtils.EMPTY;
    }

}
