/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.common;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public interface Key<T> {

    /**
     * Gets the unique identifier key.
     *
     * @return key an unique id.
     */
    T getKey();
}
