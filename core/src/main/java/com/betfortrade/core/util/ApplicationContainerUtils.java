/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.util;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.A;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public final class ApplicationContainerUtils implements Serializable {

    private AbstractComponent pageContainer;
    private Component messagesContainer;
    private AbstractComponent desktopMainContainer;
    private Window componentFromZulWrapper;

    public enum MessageType {
        ERROR("error"),
        WARNING("warning"),
        SUCCESS("success"),
        INFORMATION("info");

        private final String type;

        private MessageType(final String type) {
            this.type = type;
        }

        public String getType() {
            return this.type;
        }
    }

    public ApplicationContainerUtils() {
    }

    public void showWarningMessage(final String message) {
        this.messagesContainer.appendChild(this.buildErrorComponent(MessageType.WARNING, message));
        this.refreshMessagesContainer();
    }

    public void showWarningMessage(final List<String> messages) {
        for (final String message : messages) {
            this.messagesContainer.appendChild(this.buildErrorComponent(MessageType.WARNING, message));
            this.refreshMessagesContainer();
        }
    }

    public void showSuccessMessage(String message) {
        this.messagesContainer.appendChild(this.buildErrorComponent(MessageType.SUCCESS, message));
        this.refreshMessagesContainer();
    }

    public void showSuccessMessage(List<String> messages) {
        for (final String message : messages) {
            this.messagesContainer.appendChild(this.buildErrorComponent(MessageType.SUCCESS, message));
            this.refreshMessagesContainer();
        }
    }

    public void showInformationMessage(String messages) {
        this.messagesContainer.appendChild(this.buildErrorComponent(MessageType.INFORMATION, messages));
        this.refreshMessagesContainer();
    }

    public void showInformationMessage(List<String> messages) {
        for (final String message : messages) {
            this.messagesContainer.appendChild(this.buildErrorComponent(MessageType.INFORMATION, message));
            this.refreshMessagesContainer();
        }
    }

    public void showErrorMessage(String message) {
        this.messagesContainer.appendChild(this.buildErrorComponent(MessageType.ERROR, message));
        this.refreshMessagesContainer();
    }

    public void showErrorMessage(List<String> messages) {
        for (final String message : messages) {
            this.messagesContainer.appendChild(this.buildErrorComponent(MessageType.ERROR, message));
            this.refreshMessagesContainer();
        }
    }

    private AbstractComponent buildErrorComponent(final MessageType messageType, final String message) {
        final Div div = new Div();
        div.setSclass("alert alert-" + messageType.getType());
        div.setHflex("1");
        div.setVflex("1");
        div.setStyle("margin:2px;");
        final Label label = new Label(message);
        final A a = new A("x");
        a.setSclass("close");
        a.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            @Override
            public void onEvent(final Event event) throws Exception {
                ApplicationContainerUtils.this.messagesContainer.removeChild(div);
                ApplicationContainerUtils.this.refreshMessagesContainer();
            }
        });
        div.appendChild(a);
        div.appendChild(label);
        return div;
    }

    private void refreshMessagesContainer() {
        ApplicationContainerUtils.this.messagesContainer.getParent().invalidate();
    }

    public void openPage(String path) {
        if (this.pageContainer != null) {
            //lets remove all the container children
            Components.removeAllChildren(this.pageContainer);
            //now we can add the new page from zul
            this.pageContainer.appendChild((AbstractComponent) createComponentFromZul(path, null));
        }
    }

    public void openPageModal(String path, String action) {
        if (this.desktopMainContainer != null) {
            final AbstractComponent componentFromZul =
                    (AbstractComponent) ApplicationContainerUtils.createComponentFromZul(path, null);
            //ok, lets see, is the component you created on the zul a window?
            this.componentFromZulWrapper = new Window();
            if (componentFromZul instanceof Window) {
                //good, lets move
                this.componentFromZulWrapper = (Window) componentFromZul;
            } else {
                //so you want to do modal on a non window component!? lets wrap it in a proper window
                Component messagesContainer = componentFromZul.getFellow("messagesContainer");
                if (messagesContainer == null) {
                    messagesContainer = new Vlayout();
                    this.componentFromZulWrapper.appendChild(messagesContainer);
                }
                //set messages container on the window
                this.setMessagesContainer(messagesContainer);
                this.componentFromZulWrapper.appendChild(componentFromZul);
                this.componentFromZulWrapper.setBorder(true);
                this.componentFromZulWrapper.setTitle(this.transformActionIntoPrettyString(action));
                this.componentFromZulWrapper.setClosable(true);
                this.componentFromZulWrapper.addEventListener(Events.ON_CLOSE, new EventListener<Event>() {
                    @Override
                    public void onEvent(final Event event) throws Exception {
                        //don't know where you were before, so go to the main
                        Executions.sendRedirect("/");
                    }
                });
            }
            //ok now we are good, lets add it to layout
            this.desktopMainContainer.appendChild(this.componentFromZulWrapper);
            //now show yourself as a modal!
            this.componentFromZulWrapper.doModal();
        }
    }

    public void closeModal() {
        if (this.componentFromZulWrapper != null) {
            this.componentFromZulWrapper.detach();
        }
    }

    public void setPageContainer(final AbstractComponent pageContainer) {
        this.pageContainer = pageContainer;
    }

    public void setDesktopMainContainer(final AbstractComponent desktopMainDiv) {
        this.desktopMainContainer = desktopMainDiv;
    }

    public void setMessagesContainer(final Component messagesContainer) {
        this.messagesContainer = messagesContainer;
    }

    @SuppressWarnings("unchecked")
    public static <T> T createComponentFromZul(final String viewUrl, final Map<String, Object> params) {
        final Div parent = new Div();
        return (T) Executions.createComponents(viewUrl, parent, params);
    }

    private String transformActionIntoPrettyString(final String string) {
        return Joiner.on(" ").join(Iterables.transform(Splitter.on('-').split(string),
                new Function<String, String>() {
                    @Override
                    public String apply(final String from) {
                        return from.substring(0, 1).toUpperCase() + from.substring(1, from.length());
                    }
                }
        ));
    }
}
