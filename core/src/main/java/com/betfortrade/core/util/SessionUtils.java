/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.Principal;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Executions;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public final class SessionUtils {

    private static final Logger LOG = Logger.getLogger(SessionUtils.class);

    private static final String ANONYMOUS = "anonymous";

    private SessionUtils() {
    }

    public static String getPrincipal() {
        final Principal principal = Executions.getCurrent() != null ? Executions.getCurrent().getUserPrincipal() : null;
        String user = ANONYMOUS;
        if (principal != null) {
            user = principal.getName();
        }
        return user;
    }

    public static String getHostName() {
        String hostName = "";
        try {
            hostName = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            LOG.warn("Couldn't retrieve the host name!");
        }
        return hostName;
    }
}
