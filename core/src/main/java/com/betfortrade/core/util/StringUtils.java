/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.util;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public final class StringUtils {

    public static final String EMPTY = "";
    public static final String SPACE = " ";
}
