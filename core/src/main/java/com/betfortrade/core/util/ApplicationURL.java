/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.util;

import java.io.Serializable;
import java.util.Arrays;

import org.springframework.util.CollectionUtils;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public enum ApplicationURL implements Serializable {

    APPLICATION_HOME("/", "", "/content/page/index.zul", OpenMode.DESKTOP),
    APPLICATION_ADMIN("/content/page/index.zul?action=administration", "administration", "/content/page/admin/admin.zul", OpenMode.PAGE),
    APPLICATION_FAQ("/content/page/index.zul?action=faq", "faq", "/content/page/general/faq.zul", OpenMode.PAGE),
    APPLICATION_LOGIN("/content/page/index.zul?action=login", "login", "/content/page/user/login.zul", OpenMode.MODAL),
    APPLICATION_LOGIN_FAILED("/content/page/index.zul?action=login-failed", "login-failed", "/content/page/user/login.zul", OpenMode.MODAL),
    APPLICATION_LOGOUT("/j_spring_security_logout", "logout", "/content/page/index.zul", OpenMode.DESKTOP),
    APPLICATION_PRICING("/content/page/index.zul?action=pricing", "pricing", "/content/page/general/pricing.zul", OpenMode.PAGE),
    APPLICATION_MESSAGE("/content/page/index.zul?action=message", "message", "/content/page/message/inbox.zul", OpenMode.PAGE),
    APPLICATION_PROFILE("/content/page/index.zul?action=profile", "profile", "/content/page/user/profile.zul", OpenMode.PAGE),
    APPLICATION_PRIVACY("/content/page/index.zul?action=privacy", "privacy", "/content/page/general/privacy.zul", OpenMode.PAGE),
    APPLICATION_FEEDBACK("/content/page/index.zul?action=feedback", "feedback", "/content/page/general/feedback.zul", OpenMode.MODAL),
    APPLICATION_ABOUT("/content/page/index.zul?action=about", "about", "/content/page/general/about.zul", OpenMode.PAGE),
    APPLICATION_REGISTRATION("/content/page/index.zul?action=registration", "registration", "/content/page/user/registration.zul", OpenMode.PAGE),
    APPLICATION_REGISTRATION_VALIDATION("/content/page/index.zul?action=registration-validation&token=%{token}&email=%{email}", "registration-validation", "/content/page/user/registration-validation.zul", OpenMode.PAGE) {
        @Override
        public String getUrl(final String... parameter) {
            if (CollectionUtils.isEmpty(Arrays.asList(parameter))) {
                return this.getUrl();
            } else if (Arrays.asList(parameter).size() < 2) {
                return super.getUrl(parameter).replace("%{token}", parameter[0]).replace("%{email}", "");
            } else {
                return super.getUrl(parameter).replace("%{token}", parameter[0]).replace("%{email}", parameter[1]);
            }
        }

        @Override
        public String getUrl() {
            return super.getUrl().replace("%{token}", "").replace("%{email}", "");
        }
    },
    APPLICATION_SERVICE_TERMS("/content/page/index.zul?action=service-terms", "service-terms", "/content/page/general/service-terms.zul", OpenMode.PAGE),
    APPLICATION_CONTACTS("/content/page/index.zul?action=contacts", "contacts", "/content/page/general/contacts.zul", OpenMode.PAGE),
    APPLICATION_PASSWORD_RECOVERY("/content/page/index.zul?action=password-recovery&token=%{token}&email=%{email}", "password-recovery", "/content/page/user/password-recovery.zul", OpenMode.MODAL) {
        @Override
        public String getUrl(final String... parameter) {
            if (CollectionUtils.isEmpty(Arrays.asList(parameter))) {
                return this.getUrl();
            } else if (Arrays.asList(parameter).size() < 2) {
                return super.getUrl(parameter).replace("%{token}", parameter[0]).replace("%{email}", "");
            } else {
                return super.getUrl(parameter).replace("%{token}", parameter[0]).replace("%{email}", parameter[1]);
            }
        }

        @Override
        public String getUrl() {
            return super.getUrl().replace("%{token}", "").replace("%{email}", "");
        }
    },
    APPLICATION_SUBSCRIPTION_RENEWAL("/content/page/index.zul?action=subscription-renewal", "subscription-renewal", "/content/page/user/subscription-renewal.zul", OpenMode.MODAL) {
        @Override
        public String getUrl(final String... parameter) {
            if (CollectionUtils.isEmpty(Arrays.asList(parameter))) {
                return this.getUrl();
            } else {
                return super.getUrl(parameter).replace("%{email}", parameter[0]);
            }
        }

        @Override
        public String getUrl() {
            return super.getUrl().replace("%{email}", "");
        }
    },
    APPLICATION_NO_ACTION("/content/page/index.zul?action=no-action", "no-action", "/content/page/handler/no-action.zul", OpenMode.PAGE),
    //TODO remove after finish
    APPLICATION_COMING_SOON("/content/page/index.zul?action=coming-soon", "coming-soon", "/content/page/coming-soon.zul", OpenMode.PAGE);

    public static final String ACTION = "action";
    public static final String TOKEN = "token";

    public enum OpenMode {MODAL, PAGE, DESKTOP}

    private final String url;
    private final String zul;
    private final String action;
    private final OpenMode mode;

    private ApplicationURL(final String url, final String action, final String zul, final OpenMode mode) {
        this.url = url;
        this.zul = zul;
        this.action = action;
        this.mode = mode;
    }

    public String getUrl(final String... parameter) {
        return this.url;
    }

    public String getUrl() {
        return this.url;
    }

    public String getZul() {
        return this.zul;
    }

    public String getAction() {
        return this.action;
    }

    public OpenMode getMode() {
        return this.mode;
    }

    public static ApplicationURL resolveApplicationURLByAction(final String action) {
        ApplicationURL applicationURL = null;
        for (final ApplicationURL appURL : ApplicationURL.values()) {
            if (appURL.getAction().equalsIgnoreCase(action)) {
                applicationURL = appURL;
                break;
            }
        }
        return applicationURL;
    }
}
