/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.exception.renderer;

import com.betfortrade.core.exception.renderer.api.TypeImage;
import com.betfortrade.core.exception.renderers.generated.RendererType;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public class DefaultTypeImage implements TypeImage<RendererType> {

    @Override
    public String getImagePath(final RendererType rendererType) {
        return null;
    }
}
