/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.exception.api;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.betfortrade.core.common.Code;
import com.betfortrade.core.common.Key;
import com.betfortrade.core.resource.ResourceResolverFactory;
import com.betfortrade.core.util.StringUtils;
import com.google.common.base.Preconditions;

/**
 * This class resolves exceptions resources based on specific resource location.
 *
 * @author Manuel Martins
 */
final class ExceptionHelper {

    private static final String UNIQUE_IDENTIFIER = "[{0}]";

    private final Key<Integer> uniqueIdentifier = new ExceptionKey();
    private final List<Object> parameters = new ArrayList<Object>();
    private final Code resourceCode;

    /**
     * Default constructor.
     *
     * @param resourceCode the resource code to resolve.
     */
    ExceptionHelper(final Code<?> resourceCode) {
        this.resourceCode = Preconditions.checkNotNull(resourceCode);
    }

    /**
     * Creates a localized description of this throwable in a specified locale.
     *
     * @param locale the locale.
     * @param cause the throwable cause.
     * @param withIdentifier {@code true} to print the unique identifier.
     * @return the localized description.
     */
    @SuppressWarnings("unchecked")
    public String getLocalizedMessage(final Class<?> cause, final Locale locale, final boolean withIdentifier) {
        return MessageFormat.format("{0}{1}", ResourceResolverFactory.INSTANCE.getResourceResolver(cause, locale)
                .resolve(this.resourceCode, this.parameters), withIdentifier
                ? MessageFormat.format(UNIQUE_IDENTIFIER, this.uniqueIdentifier.getKey())
                : StringUtils.EMPTY);
    }

    /**
     * Get unique identifier for this exception.
     *
     * @return the unique identifier.
     */
    public Key<Integer> getUniqueIdentifier() {
        return this.uniqueIdentifier;
    }

    /**
     * Get parameters.
     *
     * @return parameters.
     */
    public List<Object> getParameters() {
        return this.parameters;
    }

    /**
     * Get resource code.
     *
     * @return resourceCode.
     */
    public Code getResourceCode() {
        return this.resourceCode;
    }
}
