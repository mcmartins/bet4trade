/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.exception.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import com.betfortrade.core.common.Code;
import com.betfortrade.core.common.Key;
import com.google.common.base.Objects;

/**
 * Holds utility methods to handle Runtime Exceptions.
 *
 * @author Manuel Martins
 */
public abstract class AbstractRuntimeException extends RuntimeException {

    private final ExceptionHelper exceptionHelper;

    /**
     * Default constructor.
     *
     * @param resourceCode the resource code to translate.
     * @param cause the throwable.
     */
    public AbstractRuntimeException(final Code<?> resourceCode, final Throwable cause) {
        super(cause);
        this.exceptionHelper = new ExceptionHelper(resourceCode);
    }

    @Override
    public String getLocalizedMessage() {
        return this.getLocalizedMessage(null, false);
    }

    /**
     * Creates a localized description of this throwable in a specified locale.
     *
     * @param locale the locale.
     * @param withIdentifier {@code true} to print the unique identifier.
     * @return the localized description.
     */
    public String getLocalizedMessage(final Locale locale, final boolean withIdentifier) {
        return this.exceptionHelper.getLocalizedMessage(this.getClass(), locale, withIdentifier);
    }

    /**
     * Add parameters for the localized message.
     *
     * @param parameter the parameter.
     */
    protected void addParameters(final Object... parameter) {
        if (parameter != null && Objects.firstNonNull(Arrays.asList(parameter), new ArrayList<Object>()).isEmpty()) {
            this.exceptionHelper.getParameters().addAll(Arrays.asList(parameter));
        }
    }

    /**
     * Returns the unique identifier for this exception.
     *
     * @return uniqueIdentifier the identifier.
     */
    public Key<?> getUniqueIdentifier() {
        return this.exceptionHelper.getUniqueIdentifier();
    }

    /**
     * Returns the resource code.
     *
     * @return code the resource code.
     */
    public Code getResourceCode() {
        return this.exceptionHelper.getResourceCode();
    }
}
