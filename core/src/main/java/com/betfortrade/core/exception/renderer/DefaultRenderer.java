/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.exception.renderer;

import org.zkoss.zk.ui.AbstractComponent;

import com.betfortrade.core.exception.renderer.api.MessagesContainer;
import com.betfortrade.core.exception.renderer.api.Renderer;
import com.betfortrade.core.exception.renderer.api.TypeImage;
import com.betfortrade.core.exception.renderers.generated.ExceptionRenderer;
import com.betfortrade.core.exception.renderers.generated.RendererType;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public class DefaultRenderer implements Renderer<ExceptionRenderer, AbstractComponent, RendererType> {

    @Override
    public void render(final ExceptionRenderer exceptionRenderer,
            final MessagesContainer<AbstractComponent> messagesContainer,
            final TypeImage<RendererType> typeImage) {

    }
}
