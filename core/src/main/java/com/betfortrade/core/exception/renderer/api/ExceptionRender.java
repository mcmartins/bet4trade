/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.exception.renderer.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.betfortrade.core.exception.renderers.generated.RendererType;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE})
public @interface ExceptionRender {

    String rendererClass() default "";

    boolean delegateToCause() default false;

    RendererType type() default RendererType.ERROR;

}
