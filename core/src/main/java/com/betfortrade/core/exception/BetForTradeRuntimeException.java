/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.exception;

import com.betfortrade.core.common.Code;
import com.betfortrade.core.exception.api.AbstractRuntimeException;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public class BetForTradeRuntimeException extends AbstractRuntimeException {

    /**
     * Constructs a new exception with the specified detail message.  The cause is not initialized, and may subsequently
     * be initialized by a call to {@link #initCause}.
     *
     * @param resourceCode the detail message. The detail message is saved for later retrieval by the {@link
     * #getMessage()} method.
     * @param cause the throwable exception.
     */
    public BetForTradeRuntimeException(final Code resourceCode, final Throwable cause) {
        super(resourceCode, cause);
    }
}
