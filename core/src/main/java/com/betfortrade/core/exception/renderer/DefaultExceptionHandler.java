/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.exception.renderer;

import java.lang.annotation.Annotation;

import com.betfortrade.core.exception.renderer.api.ExceptionHandler;
import com.betfortrade.core.exception.renderer.api.ExceptionRender;
import com.betfortrade.core.exception.renderers.generated.ExceptionRenderer;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public class DefaultExceptionHandler implements ExceptionHandler<ExceptionRenderer> {

    @Override
    public ExceptionRenderer handle(final Class<?> exception) {
        final Annotation[] annotations = exception.getAnnotations();
        final ExceptionRenderer exceptionRenderer = new ExceptionRenderer();
        for (final Annotation annotation : annotations) {
            if (annotation instanceof ExceptionRender) {
                final ExceptionRender myAnnotation = (ExceptionRender) annotation;
                exceptionRenderer.setDelegateToCause(myAnnotation.delegateToCause());
                exceptionRenderer.setRendererClass(myAnnotation.rendererClass());
                exceptionRenderer.setType(myAnnotation.type());
                break;
            }
        }
        return exceptionRenderer;
    }
}
