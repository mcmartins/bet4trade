/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.exception.renderer.api;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public interface TypeImage<R> {

    String getImagePath(R rendererType);

}
