/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.exception.api;

import java.util.UUID;

import com.betfortrade.core.common.Key;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
final class ExceptionKey implements Key<Integer> {

    private final int uniqueIdentifier;

    /**
     * Default constructor.
     */
    ExceptionKey() {
        final HashFunction hf = Hashing.md5();
        final HashCode hc = hf.newHasher().putString(UUID.randomUUID().toString()).hash();
        this.uniqueIdentifier = hc.asInt();
    }

    @Override
    public Integer getKey() {
        return this.uniqueIdentifier;
    }

    @Override
    public String toString() {
        return String.valueOf(this.uniqueIdentifier);
    }
}
