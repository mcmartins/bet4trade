/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.core.bean;

import java.text.MessageFormat;

import org.zkoss.zkplus.spring.SpringUtil;

import com.google.common.base.CaseFormat;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public class BeanUtil {

    private static final String BEAN_PROXY = "{0}BeanProxy";
    private static final String BEAN = "{0}Bean";

    @SuppressWarnings("unchecked")
    private static <T> T getBeanByType(final Class<T> type) {
        return (T) SpringUtil.getBean(MessageFormat.format(BEAN,
                CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, type.getSimpleName())), type);
    }

    @SuppressWarnings("unchecked")
    private static <T> T getBeanProxyByType(final Class<T> type) {
        return (T) SpringUtil.getBean(MessageFormat.format(BEAN_PROXY,
                CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, type.getSimpleName())), type);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getBean(final Class<T> type) {
        T result = getBeanProxyByType(type);
        if (result == null) {
            result = getBeanByType(type);
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public static <T> T getNativeBean(final Class<T> type) {
        return (T) SpringUtil.getBean(type.getSimpleName());
    }
}
