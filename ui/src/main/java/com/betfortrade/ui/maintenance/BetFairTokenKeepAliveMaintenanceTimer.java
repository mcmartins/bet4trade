/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.ui.maintenance;

import com.betfortrade.data.api.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class BetFairTokenKeepAliveMaintenanceTimer implements MaintenanceExecutor {

    @Autowired
    private UserService userService;

    @Override
    public void run() {
        this.userService.doPasswordRecoveryMaintenance();
    }
}
