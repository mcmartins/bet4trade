/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.ui.maintenance;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public interface MaintenanceExecutor extends Runnable {

}
