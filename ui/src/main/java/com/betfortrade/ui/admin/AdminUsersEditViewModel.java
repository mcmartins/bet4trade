package com.betfortrade.ui.admin;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.annotation.Init;

import com.betfortrade.core.bean.BeanUtil;
import com.betfortrade.data.api.entities.user.User;
import com.betfortrade.data.api.service.user.UserService;
import com.betfortrade.ui.common.AbstractViewModel;


public class AdminUsersEditViewModel extends AbstractViewModel<User> {

    private List<User> users = new ArrayList<User>();

    @Init
    public void init() {
        final UserService userService = BeanUtil.getBean(UserService.class);
        this.users.addAll(userService.getAll());
        this.selected = users.get(0);
    }

    public List<User> getUsers() {
        return users;
    }
}
