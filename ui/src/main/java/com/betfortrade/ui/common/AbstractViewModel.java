/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.ui.common;

import org.zkoss.bind.annotation.Init;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public abstract class AbstractViewModel<T> {

    protected T selected;

    @Init
    public abstract void init();

    public T getSelected() {
        return this.selected;
    }
}
