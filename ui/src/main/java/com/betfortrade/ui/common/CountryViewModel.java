/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.ui.common;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;
import org.zkoss.bind.annotation.Init;

import com.betfortrade.core.bean.BeanUtil;
import com.betfortrade.data.api.entities.general.Country;
import com.betfortrade.data.api.service.general.CountryService;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class CountryViewModel extends AbstractViewModel<Country> {

    private List<Country> countries = new ArrayList<Country>();

    @Init
    public void init() {
        final CountryService countryService = BeanUtil.getBean(CountryService.class);
        this.countries.addAll(countryService.getAll());
        if (!CollectionUtils.isEmpty(this.countries)) {
            this.selected = this.countries.get(0);
        }
    }

    public List<Country> getCountries() {
        return countries;
    }
}
