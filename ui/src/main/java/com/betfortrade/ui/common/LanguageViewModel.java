/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.ui.common;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;
import org.zkoss.bind.annotation.Init;

import com.betfortrade.core.bean.BeanUtil;
import com.betfortrade.data.api.entities.general.Language;
import com.betfortrade.data.api.service.general.LanguageService;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class LanguageViewModel extends AbstractViewModel<Language> {

    private List<Language> languages = new ArrayList<Language>();

    @Init
    public void init() {
        final LanguageService languageService = BeanUtil.getBean(LanguageService.class);
        this.languages.addAll(languageService.getAll());
        if (!CollectionUtils.isEmpty(this.languages)) {
            this.selected = this.languages.get(0);
        }
    }

    public List<Language> getLanguages() {
        return languages;
    }
}
