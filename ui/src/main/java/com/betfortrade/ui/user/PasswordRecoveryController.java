/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.ui.user;

import java.text.MessageFormat;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Div;
import org.zkoss.zul.Textbox;

import com.betfortrade.core.bean.BeanUtil;
import com.betfortrade.core.util.ApplicationContainerUtils;
import com.betfortrade.core.util.ApplicationURL;
import com.betfortrade.data.api.service.user.UserService;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class PasswordRecoveryController extends SelectorComposer<Div> {

    private static final Logger LOG = Logger.getLogger(PasswordRecoveryController.class);

    @Wire
    private Textbox email;

    @Wire
    private Textbox password;

    private String token;

    @Listen("onClick=#btnRecoverPassword")
    public void passwordRecovery() {
        final UserService userService = BeanUtil.getBean(UserService.class);
        userService.resetPassword(this.email.getValue());
        final ApplicationContainerUtils applicationContainerUtils = BeanUtil.getBean(ApplicationContainerUtils.class);
        if (LOG.isInfoEnabled()) {
            LOG.info(MessageFormat.format("Password recovery requested - {0}", this.email.getValue()));
        }
        applicationContainerUtils.showSuccessMessage("Reset password notification sent successfully!");
        applicationContainerUtils.closeModal();
    }

    @Listen("onClick=#btnChangePassword")
    public void changePassword() {
        final UserService userService = BeanUtil.getBean(UserService.class);
        userService.resetPassword(this.email.getValue(), this.token, this.password.getValue());
        final ApplicationContainerUtils applicationContainerUtils = BeanUtil.getBean(ApplicationContainerUtils.class);
        if (LOG.isInfoEnabled()) {
            LOG.info(MessageFormat.format("Password recovery finalised - {0}", this.email.getValue()));
        }
        applicationContainerUtils.showSuccessMessage("Password changed successfully!");
        applicationContainerUtils.closeModal();
    }

    @Override
    public ComponentInfo doBeforeCompose(final Page page, final Component parent, final ComponentInfo compInfo) {
        this.token = Executions.getCurrent().getParameter(ApplicationURL.TOKEN);
        return super.doBeforeCompose(page, parent, compInfo);
    }
}
