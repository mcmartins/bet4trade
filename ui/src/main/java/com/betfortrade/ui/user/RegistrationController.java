/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.ui.user;

import java.text.MessageFormat;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Textbox;

import com.betfortrade.core.bean.BeanUtil;
import com.betfortrade.core.util.ApplicationContainerUtils;
import com.betfortrade.core.util.ApplicationURL;
import com.betfortrade.data.api.service.user.UserService;
import com.betfortrade.ui.bind.ApplicationSelectorComposer;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class RegistrationController extends ApplicationSelectorComposer<Div> {

    private static final Logger LOG = Logger.getLogger(RegistrationController.class);

    @Wire("#firstName")
    private Textbox firstName;
    @Wire("#lastName")
    private Textbox lastName;
    @Wire("#preferredName")
    private Textbox preferredName;
    @Wire("#email")
    private Textbox email;
    @Wire("#username")
    private Textbox username;
    @Wire("#password")
    private Textbox password;
    @Wire("#phone")
    private Textbox phone;
    @Wire("#birthday")
    private Datebox birthday;
    @Wire("#address1")
    private Textbox address1;
    @Wire("#address2")
    private Textbox address2;
    @Wire("#city")
    private Textbox city;
    @Wire("#zipCode")
    private Textbox zipCode;
    @Wire("#country")
    private Combobox country;
    @Wire("#language")
    private Combobox language;

    @Listen("onClick=#btnSave")
    public void save() {
        final UserService userService = BeanUtil.getBean(UserService.class);
        userService.saveUser(this.firstName.getValue(), this.lastName.getValue(), this.preferredName.getValue(),
                this.birthday.getValue(), this.email.getValue(), this.phone.getValue(), this.username.getValue(),
                this.password.getValue(), this.address1.getValue(), this.address2.getValue(), this.city.getValue(),
                this.zipCode.getValue(), (String) this.country.getSelectedItem().getValue(),
                (String) this.language.getSelectedItem().getValue());
        final ApplicationContainerUtils applicationContainerUtils = BeanUtil.getBean(ApplicationContainerUtils.class);
        if (LOG.isInfoEnabled()) {
            LOG.info(MessageFormat.format("User account created - {0}", this.email.getValue()));
        }
        applicationContainerUtils.showSuccessMessage("User registered successfully!");
    }

    @Override
    public ComponentInfo doBeforeCompose(final Page page, final Component parent, final ComponentInfo compInfo) {
        final String action = Executions.getCurrent().getParameter(ApplicationURL.ACTION);
        if (ApplicationURL.APPLICATION_REGISTRATION_VALIDATION.getAction().equals(action)) {
            final String token = Executions.getCurrent().getParameter(ApplicationURL.TOKEN);
            final String email = Executions.getCurrent().getParameter("email");
            if (StringUtils.isNotBlank(token) && StringUtils.isNotBlank(email)) {
                final UserService userService = BeanUtil.getBean(UserService.class);
                userService.validateUser(email, token);
                final ApplicationContainerUtils applicationContainerUtils = BeanUtil.getBean(ApplicationContainerUtils.class);
                if (LOG.isInfoEnabled()) {
                    LOG.info(MessageFormat.format("User account validated - {0}", email));
                }
                applicationContainerUtils.showSuccessMessage("Account validated successfully!");
            } else {
                LOG.error("Someone is trying to validate an account without token and/or email or it could be trying to guess something");
                throw new RuntimeException("What are you trying to do!?");
            }
        }
        return super.doBeforeCompose(page, parent, compInfo);
    }
}
