/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.ui.general;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Div;
import org.zkoss.zul.Textbox;

import com.betfortrade.core.bean.BeanUtil;
import com.betfortrade.core.util.ApplicationContainerUtils;
import com.betfortrade.ui.bind.ApplicationSelectorComposer;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class FeedbackController extends ApplicationSelectorComposer<Div> {

    private static final Logger LOG = Logger.getLogger(FeedbackController.class);

    @Wire("#zipCode")
    private Textbox email;
    @Wire("#country")
    private Textbox name;
    @Wire("#message")
    private Textbox message;

    @Listen("onClick=#btnSend")
    public void send() {
        final ApplicationContainerUtils applicationContainerUtils = BeanUtil.getBean(ApplicationContainerUtils.class);
        applicationContainerUtils.showSuccessMessage("Feedback sent successfully!");
    }
}
