/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.ui.bind;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.SelectorComposer;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class ApplicationSelectorComposer<T extends Component> extends SelectorComposer<T> {

    @Override
    public ComponentInfo doBeforeCompose(final Page page, final Component parent,
            final ComponentInfo compInfo) {
        if (parent != null) {
            parent.setPage(page);
        }
        return super.doBeforeCompose(page, parent, compInfo);
    }
}
