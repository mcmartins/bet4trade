/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.ui.bind;

import org.zkoss.bind.BindComposer;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class ApplicationBindComposer<T extends Component> extends BindComposer<T> {

    @Override
    public ComponentInfo doBeforeCompose(final Page page, final Component parent,
            final ComponentInfo compInfo) throws Exception {
        if (parent != null) {
            parent.setPage(page);
        }
        return super.doBeforeCompose(page, parent, compInfo);
    }
}
