/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.query;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class LanguageQuery {

    /**
     * Gets a language by locale.
     *
     * @author Manuel Martins
     */
    public static final class GetLanguageByLocale {

        /**
         * Query name.
         */
        public static final String NAME = "LanguageQuery.GetLanguageByLocale";
        /**
         * The Query.
         */
        public static final String QUERY = "SELECT l FROM Language l WHERE l.languagePK.language = :language AND l.languagePK.countryPK.countryISO = :country";

        /**
         * Do not allow instance creation.
         */
        private GetLanguageByLocale() {
            // no instance.
        }
    }

}
