/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.entities.configuration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

import com.betfortrade.data.api.entities.AbstractEntity;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
@Entity
public class Configuration extends AbstractEntity<String> {

    @Id
    private String id;

    @Lob
    @Column(nullable = false)
    private String value;

    @Column(length = 255)
    private String description;

    @Column(length = 255)
    private String functionality;

    public Configuration() {
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(final String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getFunctionality() {
        return functionality;
    }

    public void setFunctionality(final String functionality) {
        this.functionality = functionality;
    }
}
