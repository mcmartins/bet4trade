/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.query;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class TemplateQuery {

    /**
     * Gets a Template By name and locale.
     *
     * @author Manuel Martins
     */
    public static final class GetTemplateByNameAndLocale {

        /**
         * Query name.
         */
        public static final String NAME = "TemplateQuery.GetTemplateByNameAndLocale";
        /**
         * The Query.
         */
        public static final String QUERY = "SELECT t FROM Template t WHERE t.templatePK.templateName = :name AND t.templatePK.languagePK.countryPK.countryISO = :country AND t.templatePK.languagePK.language = :language";

        /**
         * Do not allow instance creation.
         */
        private GetTemplateByNameAndLocale() {
            // no instance.
        }
    }


}
