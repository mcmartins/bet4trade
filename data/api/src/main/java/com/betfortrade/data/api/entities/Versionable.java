/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.entities;

import java.io.Serializable;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public interface Versionable<T> extends Serializable {

    T getVersion();

}
