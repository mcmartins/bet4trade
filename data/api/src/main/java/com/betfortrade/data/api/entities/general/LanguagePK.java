/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.entities.general;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
@Embeddable
public class LanguagePK implements Serializable {

    @Column(nullable = false, length = 2)
    private String language;

    @Embedded
    private CountryPK countryPK;

    public LanguagePK() {
    }

    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(final String language) {
        this.language = language;
    }

    public CountryPK getCountryPK() {
        return this.countryPK;
    }

    public void setCountryPK(final CountryPK country) {
        this.countryPK = country;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getCountryPK() != null ? this.getCountryPK().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof LanguagePK)) return false;
        final LanguagePK pk = (LanguagePK) obj;
        return pk.countryPK.equals(this.countryPK) && pk.language.equals(this.language);
    }
}
