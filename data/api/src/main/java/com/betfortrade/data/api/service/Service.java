/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.service;

import java.io.Serializable;
import java.util.List;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public interface Service<T> extends Serializable {

    List<T> getAll();
}
