/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.dao.bet;

import com.betfortrade.data.api.dao.DAO;
import com.betfortrade.data.api.entities.bet.BetActive;
import com.betfortrade.data.api.entities.bet.BetHistory;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public interface BetDAO extends DAO<BetActive, Long> {

    BetHistory findBetById(Long id);

    void saveBet(BetHistory bet);

    void updateBet(BetHistory bet);

    void removeBet(BetHistory bet);
}
