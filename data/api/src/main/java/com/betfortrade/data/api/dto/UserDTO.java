/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.dto;

import com.betfortrade.core.authorization.Role;
import com.betfortrade.data.api.entities.general.Country;
import com.betfortrade.data.api.entities.user.PasswordRecovery;
import com.betfortrade.data.api.entities.user.Profile;
import com.betfortrade.data.api.entities.user.Subscription;

import java.util.Date;
import java.util.Set;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public class UserDTO {

    private String username;

    private String email;

    private String firstName;

    private String lastName;

    private String preferredName;

    private Date birthDate;

    private Country country;

    private String address1;

    private String address2;

    private String city;

    private String postalCode;

    private String phoneNumber;

    private Set<Role> roles;

    private Set<Subscription> subscriptions;

    private Profile profile;

    private Set<PasswordRecovery> passwordRecoveries;

}
