/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.dao.configuration;

import com.betfortrade.data.api.dao.DAO;
import com.betfortrade.data.api.entities.configuration.Configuration;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public interface ConfigurationDAO extends DAO<Configuration, String> {

    Configuration findByKey(String key);

    void updateConfiguration(Configuration configuration);
}
