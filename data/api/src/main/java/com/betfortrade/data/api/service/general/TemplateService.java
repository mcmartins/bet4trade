/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.service.general;

import java.util.Locale;

import org.joda.time.DateTime;

import com.betfortrade.data.api.entities.general.Template;
import com.betfortrade.data.api.service.Service;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public interface TemplateService extends Service<Template> {

    String getTemplate(String name, Locale locale);

    DateTime getTemplateLastModifiedDate(String name);
}
