/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.interceptor;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

import org.joda.time.DateTime;

import com.betfortrade.core.util.SessionUtils;
import com.betfortrade.data.api.entities.Auditable;

/**
 * Interceptor used to fill audit columns.
 *
 * @author Manuel Martins
 */
public class AuditInterceptor {

    @PrePersist
    public void auditPersistEntity(final Object entityObj) {
        if (entityObj instanceof Auditable) {
            final Auditable entity = (Auditable) entityObj;
            entity.setCreationDate(new DateTime());
            entity.setCreatedBy(SessionUtils.getPrincipal());
            this.auditUpdateEntity(entityObj);
        }
    }

    @PreUpdate
    @PreRemove
    public void auditUpdateEntity(final Object entityObj) {
        if (entityObj instanceof Auditable) {
            final Auditable entity = (Auditable) entityObj;
            entity.setChangedDate(new DateTime());
            entity.setChangedBy(SessionUtils.getPrincipal());
        }
    }
}
