/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.query;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class UserQuery {

    /**
     * Gets an User by email.
     *
     * @author Manuel Martins
     */
    public static final class GetUserByEmail {

        /**
         * Query name.
         */
        public static final String NAME = "UserQuery.GetUserByEmail";
        /**
         * The Query.
         */
        public static final String QUERY = "SELECT u FROM User u WHERE u.email = :email";

        /**
         * Do not allow instance creation.
         */
        private GetUserByEmail() {
            // no instance.
        }
    }

    /**
     * Gets a Subscription by user email.
     *
     * @author Manuel Martins
     */
    public static final class GetSubscriptionByEmail {

        /**
         * Query name.
         */
        public static final String NAME = "UserQuery.GetSubscriptionByEmail";
        /**
         * The Query.
         */
        public static final String QUERY = "SELECT s FROM Subscription s JOIN s.user u WHERE u.email = :email AND s.lastSubscription = true";

        /**
         * Do not allow instance creation.
         */
        private GetSubscriptionByEmail() {
            // no instance.
        }
    }

    /**
     * Gets a Subscription by user email.
     *
     * @author Manuel Martins
     */
    public static final class GetPasswordRecoveryByEmail {

        /**
         * Query name.
         */
        public static final String NAME = "UserQuery.GetPasswordRecoveryByEmail";
        /**
         * The Query.
         */
        public static final String QUERY = "SELECT p FROM PasswordRecovery p JOIN p.user u WHERE u.email = :email AND p.passwordRecoveryExpired = false AND p.passwordRecoveryValidated = false";

        /**
         * Do not allow instance creation.
         */
        private GetPasswordRecoveryByEmail() {
            // no instance.
        }
    }
}
