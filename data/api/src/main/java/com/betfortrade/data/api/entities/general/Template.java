/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.entities.general;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;

import com.betfortrade.data.api.entities.AbstractEntity;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
@Entity
public class Template extends AbstractEntity<TemplatePK> {

    @EmbeddedId
    private TemplatePK templatePK;

    @Lob
    @Column(nullable = false)
    private String templateBody;

    public Template() {
    }

    @Override
    public TemplatePK getId() {
        return templatePK;
    }

    @Override
    public void setId(final TemplatePK languagePK) {
        this.templatePK = languagePK;
    }

    public String getTemplateBody() {
        return templateBody;
    }

    public void setTemplateBody(final String resource) {
        this.templateBody = resource;
    }
}
