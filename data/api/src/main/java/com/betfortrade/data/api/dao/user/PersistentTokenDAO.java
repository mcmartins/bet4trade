/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.dao.user;

import java.io.Serializable;
import java.util.Date;

import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public interface PersistentTokenDAO extends Serializable {

    public void insertToken(PersistentRememberMeToken token);

    public void updateToken(String series, String tokenValue, Date lastUsed);

    public void deleteToken(String username);

    public PersistentRememberMeToken getTokenForSeries(String seriesId);
}
