/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.query;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class PersistentLoginQuery {

    /**
     * Gets a language by locale.
     *
     * @author Manuel Martins
     */
    public static final class GetPersistentLoginByUsername {

        /**
         * Query name.
         */
        public static final String NAME = "PersistentLoginQuery.GetPersistentLoginByUsername";
        /**
         * The Query.
         */
        public static final String QUERY = "SELECT p FROM PersistentLogin p WHERE p.username = :username";

        /**
         * Do not allow instance creation.
         */
        private GetPersistentLoginByUsername() {
            // no instance.
        }
    }
}
