/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.query;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class SubscriptionQuery {

    /**
     * Gets an User by email.
     *
     * @author Manuel Martins
     */
    public static final class GetAllActiveSubscriptionsExpiring {

        /**
         * Query name.
         */
        public static final String NAME = "SubscriptionQuery.GetAllActiveSubscriptions";
        /**
         * The Query.
         */
        public static final String QUERY = "SELECT s FROM Subscription s WHERE s.lastSubscription = true AND s.subscriptionValid = true AND s.subscriptionEnd < :dateTime";

        /**
         * Do not allow instance creation.
         */
        private GetAllActiveSubscriptionsExpiring() {
            // no instance.
        }
    }
}
