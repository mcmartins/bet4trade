/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.query;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class PasswordRecoveryQuery {

    /**
     * Gets a Subscription by user email.
     *
     * @author Manuel Martins
     */
    public static final class GetAllPasswordRecoveryExpired {

        /**
         * Query name.
         */
        public static final String NAME = "PasswordRecoveryQuery.GetAllPasswordRecoveryExpired";
        /**
         * The Query.
         */
        public static final String QUERY = "SELECT p FROM PasswordRecovery p WHERE p.passwordRecoveryValidated = false AND p.passwordRecoveryExpired = false AND p.creationDate < :dateTime";

        /**
         * Do not allow instance creation.
         */
        private GetAllPasswordRecoveryExpired() {
            // no instance.
        }
    }
}
