/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.entities;

import java.io.Serializable;

/**
 * @author Manuel Martins
 */
public interface Stateable extends Serializable {

    Boolean getDisabled();

    void setDisabled(Boolean disabled);

}
