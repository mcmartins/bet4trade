/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.entities.general;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import org.springframework.data.domain.Sort;

import com.betfortrade.data.api.entities.AbstractEntity;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
@Entity
public class Language extends AbstractEntity<LanguagePK> {

    @EmbeddedId
    private LanguagePK languagePK;

    @Column(length = 100)
    private String prettyName;

    public Language() {
    }

    @Override
    public LanguagePK getId() {
        return languagePK;
    }

    @Override
    public void setId(final LanguagePK id) {
        this.languagePK = id;
    }

    public String getPrettyName() {
        return prettyName;
    }

    public void setPrettyName(final String prettyName) {
        this.prettyName = prettyName;
    }

    public static enum OrderField {
        PRETTY_NAME("prettyName");

        private String fieldName;

        private OrderField(final String fieldName) {
            this.fieldName = fieldName;
        }

        public Sort getAscSort() {
            return new Sort(new Sort.Order(Sort.Direction.ASC, this.fieldName));
        }

        public Sort getDescSort() {
            return new Sort(new Sort.Order(Sort.Direction.DESC, this.fieldName));
        }

        public String getFieldName() {
            return fieldName;
        }
    }
}
