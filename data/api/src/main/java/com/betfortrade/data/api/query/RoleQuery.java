/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.query;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class RoleQuery {

    /**
     * Gets a Role by name.
     *
     * @author Manuel Martins
     */
    public static final class GetRoleByName {

        /**
         * Query name.
         */
        public static final String NAME = "RoleQuery.GetRoleByName";
        /**
         * The Query.
         */
        public static final String QUERY = "SELECT r FROM Role r WHERE r.name = :name";

        /**
         * Do not allow instance creation.
         */
        private GetRoleByName() {
            // no instance.
        }
    }


}
