/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.dao.user;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.betfortrade.data.api.dao.DAO;
import com.betfortrade.data.api.entities.general.Language;
import com.betfortrade.data.api.entities.user.User;
import com.betfortrade.data.api.query.UserQuery;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public interface UserDAO extends DAO<User, Long> {

    @Query(UserQuery.GetUserByEmail.QUERY)
    User findUserByEmail(@Param("email") String email);

    @Query(UserQuery.GetUserByEmail.QUERY)
    Language getLanguageFromProfile(@Param("email") String email);
}
