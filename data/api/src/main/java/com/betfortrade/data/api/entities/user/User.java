/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.entities.user;

import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.betfortrade.data.api.entities.AbstractEntity;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
@Entity
public class User extends AbstractEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true, length = 255, nullable = false)
    private String email;

    @Column(unique = true, length = 100, nullable = false)
    private String username;

    @Column(length = 255, nullable = false)
    private String password;

    @OneToOne(cascade = {CascadeType.ALL})
    private Person person;

    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private Set<Role> roles;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private Set<Subscription> subscriptions;

    @OneToOne(cascade = {CascadeType.ALL})
    private Profile profile;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private Set<PasswordRecovery> passwordRecoveries;

    public User() {
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(final Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(final Person person) {
        this.person = person;
    }

    public Set<Role> getRoles() {
        if (this.roles == null) {
            this.roles = new TreeSet<Role>();
        }
        return this.roles;
    }

    public void setRoles(final Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Subscription> getSubscriptions() {
        if (this.subscriptions == null) {
            this.subscriptions = new TreeSet<Subscription>();
        }
        return this.subscriptions;
    }

    public void setSubscriptions(final Set<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(final Profile profile) {
        this.profile = profile;
    }

    public Set<PasswordRecovery> getPasswordRecoveries() {
        if (this.passwordRecoveries == null) {
            this.passwordRecoveries = new TreeSet<PasswordRecovery>();
        }
        return this.passwordRecoveries;
    }

    public void setPasswordRecoveries(final Set<PasswordRecovery> passwordRecoveries) {
        this.passwordRecoveries = passwordRecoveries;
    }
}
