/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.entities;

import java.io.Serializable;

import org.joda.time.DateTime;

/**
 * Auditable Entity Interface.
 *
 * @author Manuel Martins
 */
public interface Auditable extends Serializable {

    String getChangedBy();

    void setChangedBy(String changeBy);

    DateTime getChangedDate();

    void setChangedDate(DateTime changeDate);

    String getCreatedBy();

    void setCreatedBy(String createBy);

    DateTime getCreationDate();

    void setCreationDate(DateTime createDate);

}
