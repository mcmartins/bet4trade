/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.entities.user;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.betfortrade.data.api.entities.AbstractEntity;
import com.betfortrade.data.api.entities.SurrogatePK;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
@Entity
public class PasswordRecovery extends AbstractEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(cascade = {CascadeType.ALL})
    private User user;

    @Column(length = 100)
    private String passwordRecoveryToken;

    @Column
    private boolean passwordRecoveryValidated;

    @Column
    private boolean passwordRecoveryExpired;

    public PasswordRecovery() {
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(final Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(final User user) {
        this.user = user;
    }

    public String getPasswordRecoveryToken() {
        return passwordRecoveryToken;
    }

    public void setPasswordRecoveryToken(final String passwordRecoveryToken) {
        this.passwordRecoveryToken = passwordRecoveryToken;
    }

    public boolean isPasswordRecoveryValidated() {
        return passwordRecoveryValidated;
    }

    public void setPasswordRecoveryValidated(final boolean passwordRecoveryValidated) {
        this.passwordRecoveryValidated = passwordRecoveryValidated;
    }

    public boolean isPasswordRecoveryExpired() {
        return passwordRecoveryExpired;
    }

    public void setPasswordRecoveryExpired(final boolean passwordRecoveryExpired) {
        this.passwordRecoveryExpired = passwordRecoveryExpired;
    }
}
