/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.entities.user;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.joda.time.DateTime;

import com.betfortrade.data.api.entities.AbstractEntity;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
@Entity
public class Subscription extends AbstractEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date subscriptionStart;

    @Column(nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date subscriptionEnd;

    @Column(nullable = false, length = 255)
    private String subscriptionToken;

    @Column(nullable = false)
    private boolean subscriptionValid;

    @Column(nullable = false)
    private boolean lastSubscription;

    @Column(nullable = false)
    private boolean notifiedSubscriptionExpired;

    @Column(nullable = false)
    private boolean notifiedSubscriptionExpiring;

    @ManyToOne(cascade = {CascadeType.ALL})
    private User user;

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(final Long id) {
        this.id = id;
    }

    public DateTime getSubscriptionStart() {
        return null == this.subscriptionStart ? null : new DateTime(this.subscriptionStart);
    }

    public void setSubscriptionStart(final DateTime subscriptionStart) {
        this.subscriptionStart = null == subscriptionStart ? null : subscriptionStart.toDate();
    }

    public DateTime getSubscriptionEnd() {
        return null == this.subscriptionEnd ? null : new DateTime(this.subscriptionEnd);
    }

    public void setSubscriptionEnd(final DateTime subscriptionEnd) {
        this.subscriptionEnd = null == subscriptionEnd ? null : subscriptionEnd.toDate();
    }

    public boolean isSubscriptionValid() {
        return subscriptionValid;
    }

    public void setSubscriptionValid(final boolean subscriptionValid) {
        this.subscriptionValid = subscriptionValid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(final User user) {
        this.user = user;
    }

    public String getSubscriptionToken() {
        return subscriptionToken;
    }

    public void setSubscriptionToken(final String subscriptionToken) {
        this.subscriptionToken = subscriptionToken;
    }

    public boolean isLastSubscription() {
        return lastSubscription;
    }

    public void setLastSubscription(final boolean lastSubscription) {
        this.lastSubscription = lastSubscription;
    }

    public boolean setNotifiedSubscriptionExpired() {
        return notifiedSubscriptionExpired;
    }

    public void setNotifiedSubscriptionExpired(final boolean expiredNotified) {
        this.notifiedSubscriptionExpired = expiredNotified;
    }

    public boolean setNotifiedSubscriptionExpiring() {
        return notifiedSubscriptionExpiring;
    }

    public void setNotifiedSubscriptionExpiring(final boolean expiringNotified) {
        this.notifiedSubscriptionExpiring = expiringNotified;
    }
}
