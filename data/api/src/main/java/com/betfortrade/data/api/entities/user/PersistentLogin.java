/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.entities.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.betfortrade.data.api.entities.Identifiable;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
@Entity
public class PersistentLogin implements Identifiable<String> {

    @Id
    @Column(length = 100, nullable = false)
    private String series;

    @Column(length = 100, nullable = false)
    private String username;

    @Column(length = 100, nullable = false)
    private String token;

    @Column(nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUsed;

    @Override
    public String getId() {
        return this.series;
    }

    @Override
    public void setId(final String series) {
        this.series = series;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(final String token) {
        this.token = token;
    }

    public Date getLastUsed() {
        return lastUsed;
    }

    public void setLastUsed(final Date lastUsed) {
        this.lastUsed = lastUsed;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Identifiable)) {
            return false;
        }
        final Identifiable other = (Identifiable) object;
        return ((this.getId() == null && other.getId() != null)
                || (this.getId() != null && !this.getId().equals(other.getId())));
    }

    @Override
    public String toString() {
        return String.format("[EntityID=%s]", this.getId());
    }
}
