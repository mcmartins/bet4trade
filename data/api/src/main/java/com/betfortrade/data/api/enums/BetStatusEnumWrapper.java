/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.enums;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public enum BetStatusEnumWrapper {

    U, M, S, C, V, L, MU;

    public String value() {
        return name();
    }

    public static BetStatusEnumWrapper fromValue(String v) {
        return valueOf(v);
    }
}
