/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.joda.time.DateTime;

import com.betfortrade.data.api.interceptor.AuditInterceptor;

/**
 * All Entities must extend this.
 *
 * @author Manuel Martins
 */
@MappedSuperclass
@EntityListeners(AuditInterceptor.class)
abstract class AbstractAuditEntity implements Auditable {

    @Column(name = "creationDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @Column(name = "createdBy")
    private String createdBy;

    @Column(name = "changedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date changedDate;

    @Column(name = "changedBy")
    private String changedBy;

    @Override
    public String getChangedBy() {
        return changedBy;
    }

    @Override
    public void setChangedBy(String changeBy) {
        this.changedBy = changeBy;
    }

    @Override
    public DateTime getChangedDate() {
        return null == this.changedDate ? null : new DateTime(this.changedDate);
    }

    @Override
    public void setChangedDate(DateTime changeDate) {
        this.changedDate = null == changeDate ? null : changeDate.toDate();
    }

    @Override
    public String getCreatedBy() {
        return createdBy;
    }

    @Override
    public void setCreatedBy(String createBy) {
        this.createdBy = createBy;
    }

    @Override
    public DateTime getCreationDate() {
        return null == this.creationDate ? null : new DateTime(this.creationDate);
    }

    @Override
    public void setCreationDate(DateTime createDate) {
        this.creationDate = null == createDate ? null : createDate.toDate();
    }
}
