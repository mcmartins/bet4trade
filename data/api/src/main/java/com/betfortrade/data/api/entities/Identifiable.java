/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.entities;

import java.io.Serializable;

/**
 * Ids from Entities must override this methods.
 *
 * @author Manuel Martins
 */
public interface Identifiable<T> extends Serializable {

    T getId();

    void setId(T id);

}
