/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.dao.user;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.betfortrade.data.api.dao.DAO;
import com.betfortrade.data.api.entities.user.PasswordRecovery;
import com.betfortrade.data.api.query.PasswordRecoveryQuery;
import com.betfortrade.data.api.query.UserQuery;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public interface PasswordRecoveryDAO extends DAO<PasswordRecovery, Long> {

    @Query(UserQuery.GetPasswordRecoveryByEmail.QUERY)
    PasswordRecovery findPasswordRecoveryByEmail(@Param("email") String email);

    @Query(PasswordRecoveryQuery.GetAllPasswordRecoveryExpired.QUERY)
    List<PasswordRecovery> findAllPasswordRecoveryExpired(@Param("dateTime") Date dateTime);
}
