/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.dao.user;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.betfortrade.data.api.dao.DAO;
import com.betfortrade.data.api.entities.user.Role;
import com.betfortrade.data.api.query.RoleQuery;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public interface RoleDAO extends DAO<Role, Long> {

    @Query(RoleQuery.GetRoleByName.QUERY)
    Role findRoleByName(@Param("name") String name);
}
