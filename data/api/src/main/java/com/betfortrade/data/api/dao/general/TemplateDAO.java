/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.dao.general;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.betfortrade.data.api.dao.DAO;
import com.betfortrade.data.api.entities.general.Template;
import com.betfortrade.data.api.entities.general.TemplatePK;
import com.betfortrade.data.api.query.TemplateQuery;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public interface TemplateDAO extends DAO<Template, TemplatePK> {

    @Query(TemplateQuery.GetTemplateByNameAndLocale.QUERY)
    Template getTemplate(@Param("name") String name, @Param("country") String country,
            @Param("language") String language);
}
