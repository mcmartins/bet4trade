/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.entities.user;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.joda.time.DateTime;

import com.betfortrade.data.api.entities.AbstractEntity;
import com.betfortrade.data.api.entities.SurrogatePK;
import com.betfortrade.data.api.entities.general.Country;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
@Entity
public class Person extends AbstractEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 255, nullable = false)
    private String firstName;

    @Column(length = 255, nullable = false)
    private String lastName;

    @Column(length = 100, nullable = true)
    private String preferredName;

    @Column(nullable = true)
    @Temporal(TemporalType.DATE)
    private Date birthDate;

    @ManyToOne
    private Country country;

    @Column(length = 255, nullable = false)
    private String address1;

    @Column(length = 255, nullable = true)
    private String address2;

    @Column(length = 255, nullable = true)
    private String city;

    @Column(length = 10, nullable = false)
    private String postalCode;

    @Column(length = 25, nullable = false)
    private String phoneNumber;

    @OneToOne(cascade = {CascadeType.ALL})
    private User user;

    public Person() {
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(final Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getPreferredName() {
        return preferredName;
    }

    public void setPreferredName(final String preferredName) {
        this.preferredName = preferredName;
    }

    public DateTime getBirthDate() {
        return null == this.birthDate ? null : new DateTime(this.birthDate);
    }

    public void setBirthDate(final DateTime birthDate) {
        this.birthDate = null == birthDate ? null : birthDate.toDate();
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(final Country country) {
        this.country = country;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(final String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(final String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(final String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public User getUser() {
        return user;
    }

    public void setUser(final User user) {
        this.user = user;
    }
}
