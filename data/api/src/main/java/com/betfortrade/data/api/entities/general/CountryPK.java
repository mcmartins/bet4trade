/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.entities.general;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
@Embeddable
public class CountryPK implements Serializable {

    @Column(nullable = false, length = 2)
    private String countryISO;

    public CountryPK() {
    }

    public String getCountryISO() {
        return countryISO;
    }

    public void setCountryISO(final String iso) {
        this.countryISO = iso;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getCountryISO() != null ? this.getCountryISO().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof CountryPK)) return false;
        final CountryPK pk = (CountryPK) obj;
        return pk.getCountryISO().equals(this.countryISO);
    }
}
