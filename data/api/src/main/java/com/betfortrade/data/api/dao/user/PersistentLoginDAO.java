/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.dao.user;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.betfortrade.data.api.dao.DAO;
import com.betfortrade.data.api.entities.user.PersistentLogin;
import com.betfortrade.data.api.query.PersistentLoginQuery;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public interface PersistentLoginDAO extends DAO<PersistentLogin, String> {

    @Query(PersistentLoginQuery.GetPersistentLoginByUsername.QUERY)
    public PersistentLogin findByUsername(@Param("username") String username);
}
