/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.dao.general;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.betfortrade.data.api.dao.DAO;
import com.betfortrade.data.api.entities.general.Language;
import com.betfortrade.data.api.entities.general.LanguagePK;
import com.betfortrade.data.api.query.LanguageQuery;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public interface LanguageDAO extends DAO<Language, LanguagePK> {

    @Query(LanguageQuery.GetLanguageByLocale.QUERY)
    Language getLanguageByLocale(@Param("country") String country, @Param("language") String language);
}
