/*
 * Copyright (c) BetForTrade, All Rights Reserved.
 * (www.betfortrade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.entities;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
@Embeddable
public class SurrogatePK<T extends Number> implements Identifiable<T> {

    @GeneratedValue(strategy = GenerationType.AUTO)
    private T id;

    public SurrogatePK() {
    }

    @Override
    public T getId() {
        return id;
    }

    @Override
    public void setId(final T id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof SurrogatePK)) return false;
        final SurrogatePK pk = (SurrogatePK) obj;
        return pk.id.equals(this.id);
    }
}
