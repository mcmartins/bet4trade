/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.entities.general;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import org.springframework.data.domain.Sort;

import com.betfortrade.data.api.entities.AbstractEntity;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
@Entity
public class Country extends AbstractEntity<CountryPK> {

    @EmbeddedId
    private CountryPK countryPK;

    @Column(length = 100)
    private String prettyName;

    @Column(length = 3)
    private String countryISO3;

    @Column(length = 4)
    private String countryNumericCode;

    @Column(nullable = false, length = 100)
    private String name;

    public Country() {
    }

    @Override
    public CountryPK getId() {
        return countryPK;
    }

    @Override
    public void setId(final CountryPK id) {
        this.countryPK = id;
    }

    public CountryPK getCountryPK() {
        return countryPK;
    }

    public void setCountryPK(final CountryPK countryPK) {
        this.countryPK = countryPK;
    }

    public String getPrettyName() {
        return prettyName;
    }

    public void setPrettyName(final String prettyName) {
        this.prettyName = prettyName;
    }

    public String getCountryISO3() {
        return countryISO3;
    }

    public void setCountryISO3(final String iso3) {
        this.countryISO3 = iso3;
    }

    public String getCountryNumericCode() {
        return countryNumericCode;
    }

    public void setCountryNumericCode(final String numCode) {
        this.countryNumericCode = numCode;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public static enum OrderField {
        NAME("name");

        private String fieldName;

        private OrderField(final String fieldName) {
            this.fieldName = fieldName;
        }

        public Sort getAscSort() {
            return new Sort(new Sort.Order(Sort.Direction.ASC, this.fieldName));
        }

        public Sort getDescSort() {
            return new Sort(new Sort.Order(Sort.Direction.DESC, this.fieldName));
        }

        public String getFieldName() {
            return fieldName;
        }
    }
}
