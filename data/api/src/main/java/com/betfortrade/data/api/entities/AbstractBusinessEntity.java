/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.entities;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

/**
 * This class holds the three required common persistence methods: hashCode, toString, equals.
 *
 * @author Manuel Martins
 */
@MappedSuperclass
public abstract class AbstractBusinessEntity<T extends Serializable>
        extends AbstractEntity<T> implements BusinessIdentifiable<T> {

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash += (this.getBusinessId() != null ? this.getBusinessId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Identifiable)) {
            return false;
        }
        final Identifiable other = (Identifiable) object;
        return ((this.getBusinessId() == null && other.getId() != null)
                || (this.getBusinessId() != null && !this.getBusinessId().equals(other.getId())));
    }

    @Override
    public String toString() {
        return String.format("[EntityBusinessID=%s]", this.getBusinessId());
    }
}
