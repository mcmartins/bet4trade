/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.dao.general;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.betfortrade.data.api.dao.DAO;
import com.betfortrade.data.api.entities.general.Country;
import com.betfortrade.data.api.entities.general.CountryPK;
import com.betfortrade.data.api.query.CountryQuery;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public interface CountryDAO extends DAO<Country, CountryPK> {

    @Query(CountryQuery.GetCountryByISO.QUERY)
    Country getCountryByISO(@Param("countryISO") String countryISO);
}
