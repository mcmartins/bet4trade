/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.entities.general;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
@Embeddable
public class TemplatePK implements Serializable {

    @Embedded
    private LanguagePK languagePK;

    @Column(nullable = false, length = 255)
    private String templateName;

    public TemplatePK() {
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(final String resourceKey) {
        this.templateName = resourceKey;
    }

    public LanguagePK getLanguagePK() {
        return this.languagePK;
    }

    public void setLanguagePK(final LanguagePK country) {
        this.languagePK = country;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getLanguagePK() != null ? this.getLanguagePK().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof TemplatePK)) return false;
        final TemplatePK pk = (TemplatePK) obj;
        return pk.languagePK.equals(this.languagePK) && pk.templateName.equals(this.templateName);
    }
}
