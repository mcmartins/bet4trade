/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.query;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class CountryQuery {

    /**
     * Gets a country by ISO code.
     *
     * @author Manuel Martins
     */
    public static final class GetCountryByISO {

        /**
         * Query name.
         */
        public static final String NAME = "CountryQuery.GetCountryByISO";
        /**
         * The Query.
         */
        public static final String QUERY = "SELECT c FROM Country c WHERE c.countryPK.countryISO = :countryISO";

        /**
         * Do not allow instance creation.
         */
        private GetCountryByISO() {
            // no instance.
        }
    }

}
