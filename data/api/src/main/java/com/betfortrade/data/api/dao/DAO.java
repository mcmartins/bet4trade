/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public interface DAO<T, K extends Serializable> extends JpaRepository<T, K>, Serializable {
}
