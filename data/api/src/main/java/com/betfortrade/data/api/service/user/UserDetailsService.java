/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.service.user;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public interface UserDetailsService extends org.springframework.security.core.userdetails.UserDetailsService {
}
