/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.service.general;

import com.betfortrade.data.api.entities.general.Country;
import com.betfortrade.data.api.service.Service;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public interface CountryService extends Service<Country> {
}
