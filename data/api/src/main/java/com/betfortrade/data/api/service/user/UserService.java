/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.service.user;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.betfortrade.data.api.entities.user.User;
import com.betfortrade.data.api.service.Service;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public interface UserService extends Service<User> {

    User getUserByEmail(String email);

    void saveUser(String firstName, String lastName, String preferredName, Date birthDate,
            String email, String phone, String username, String password, String address1,
            String address2, String city, String zipCode, String country, String language);

    boolean validateUser(String email, String validationToken);

    String resetPassword(String email);

    void resetPassword(String email, String token, String newPassword);

    void doSubscriptionMaintenance();

    void doPasswordRecoveryMaintenance();

    Locale getUserLanguage(String email);

    boolean isSubscriptionValid(String email);

    String getNewToken();
}
