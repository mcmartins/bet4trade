/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.api.entities.bet;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.betfortrade.data.api.entities.AbstractEntity;
import com.betfortrade.data.api.entities.SurrogatePK;
import com.betfortrade.data.api.enums.BetCategoryTypeEnumWrapper;
import com.betfortrade.data.api.enums.BetPersistenceTypeEnumWrapper;
import com.betfortrade.data.api.enums.BetStatusEnumWrapper;
import com.betfortrade.data.api.enums.BetTypeEnumWrapper;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
@Entity
public class BetHistory extends AbstractEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private Integer betId;

    @Column
    private Integer asianLineId;

    @Column
    private Integer marketId;

    @Column
    private Integer selectionId;

    @Column
    private Double price;

    @Column
    private Double size;

    @Column
    private Double bspLiability;

    @Column
    private Double tradePrice;

    @Column
    private Double tradeSize;

    @Column
    private String description;

    @Column
    private Boolean successTrade;

    @Column
    private BetTypeEnumWrapper betType;

    @Column
    private BetCategoryTypeEnumWrapper betCategoryType;

    @Column
    private BetPersistenceTypeEnumWrapper betPersistenceType;

    @Column
    private BetStatusEnumWrapper matchedUnmatched;

    @Column
    private Date marketCloseDate;

    public BetHistory() {
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(final Long id) {
        this.id = id;
    }

    public Integer getBetId() {
        return betId;
    }

    public void setBetId(final Integer betId) {
        this.betId = betId;
    }

    public int getAsianLineId() {
        return asianLineId;
    }

    public void setAsianLineId(int asianLineId) {
        this.asianLineId = asianLineId;
    }

    public BetTypeEnumWrapper getBetType() {
        return betType;
    }

    public void setBetType(BetTypeEnumWrapper betType) {
        this.betType = betType;
    }

    public BetCategoryTypeEnumWrapper getBetCategoryType() {
        return betCategoryType;
    }

    public void setBetCategoryType(BetCategoryTypeEnumWrapper betCategoryType) {
        this.betCategoryType = betCategoryType;
    }

    public BetPersistenceTypeEnumWrapper getBetPersistenceType() {
        return betPersistenceType;
    }

    public void setBetPersistenceType(BetPersistenceTypeEnumWrapper betPersistenceType) {
        this.betPersistenceType = betPersistenceType;
    }

    public int getMarketId() {
        return marketId;
    }

    public void setMarketId(int marketId) {
        this.marketId = marketId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int getSelectionId() {
        return selectionId;
    }

    public void setSelectionId(int selectionId) {
        this.selectionId = selectionId;
    }

    public Double getSize() {
        return size;
    }

    public void setSize(Double size) {
        this.size = size;
    }

    public Double getBspLiability() {
        return bspLiability;
    }

    public void setBspLiability(Double bspLiability) {
        this.bspLiability = bspLiability;
    }

    public Date getMarketCloseDate() {
        return marketCloseDate;
    }

    public void setMarketCloseDate(final Date marketCloseDate) {
        this.marketCloseDate = marketCloseDate;
    }

    public Double getTradePrice() {
        return tradePrice;
    }

    public void setTradePrice(final Double tradePrice) {
        this.tradePrice = tradePrice;
    }

    public Double getTradeSize() {
        return tradeSize;
    }

    public void setTradeSize(final Double tradeSize) {
        this.tradeSize = tradeSize;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Boolean getSuccessTrade() {
        return successTrade;
    }

    public void setSuccessTrade(final Boolean successTrade) {
        this.successTrade = successTrade;
    }

    public BetStatusEnumWrapper getMatchedUnmatched() {
        return matchedUnmatched;
    }

    public void setMatchedUnmatched(final BetStatusEnumWrapper matchedUnmatched) {
        this.matchedUnmatched = matchedUnmatched;
    }

    public class SystemBetNamedQueries {

        public static final String FIND_BY_TRANSACTION_TYPE = "SELECT b FROM Bet b WHERE b.successTrade = false AND b.transactionBetType = :type";
    }
}
