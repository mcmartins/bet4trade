/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.impl.general;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;

import com.betfortrade.core.cache.CacheNames;
import com.betfortrade.data.api.dao.general.CountryDAO;
import com.betfortrade.data.api.entities.general.Country;
import com.betfortrade.data.api.service.general.CountryService;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class CountryServiceImpl implements CountryService {

    @Autowired
    private CountryDAO countryDAO;

    @Override
    @Cacheable(CacheNames.COUNTRY)
    public List<Country> getAll() {
        return this.countryDAO.findAll(Country.OrderField.NAME.getAscSort());
    }
}
