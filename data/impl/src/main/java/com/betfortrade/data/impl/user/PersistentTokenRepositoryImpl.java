/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.impl.user;

import java.util.Date;

import com.betfortrade.data.api.dao.user.PersistentTokenDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class PersistentTokenRepositoryImpl implements PersistentTokenRepository {

    @Autowired
    private PersistentTokenDAO persistentTokenDao;

    @Override
    public void createNewToken(final PersistentRememberMeToken token) {
        this.persistentTokenDao.insertToken(token);
    }

    @Override
    public PersistentRememberMeToken getTokenForSeries(final String seriesId) {
        return this.persistentTokenDao.getTokenForSeries(seriesId);
    }

    @Override
    public void removeUserTokens(final String username) {
        this.persistentTokenDao.deleteToken(username);
    }

    @Override
    public void updateToken(final String series, final String tokenValue, final Date lastUsed) {
        this.persistentTokenDao.updateToken(series, tokenValue, lastUsed);
    }

}
