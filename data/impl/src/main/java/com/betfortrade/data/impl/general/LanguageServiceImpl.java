/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.impl.general;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;

import com.betfortrade.core.cache.CacheNames;
import com.betfortrade.data.api.dao.general.LanguageDAO;
import com.betfortrade.data.api.entities.general.Language;
import com.betfortrade.data.api.service.general.LanguageService;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class LanguageServiceImpl implements LanguageService {

    @Autowired
    private LanguageDAO languageDAO;

    @Override
    @Cacheable(CacheNames.LANGUAGE)
    public List<Language> getAll() {
        return this.languageDAO.findAll(Language.OrderField.PRETTY_NAME.getAscSort());
    }
}
