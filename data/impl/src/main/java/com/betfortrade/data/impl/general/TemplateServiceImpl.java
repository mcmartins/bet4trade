/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.impl.general;

import java.util.List;
import java.util.Locale;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;

import com.betfortrade.core.cache.CacheNames;
import com.betfortrade.data.api.dao.general.TemplateDAO;
import com.betfortrade.data.api.entities.general.Template;
import com.betfortrade.data.api.service.general.TemplateService;

/**
 * <Description>.
 *
 * @author Manuel Martins
 */
public class TemplateServiceImpl implements TemplateService {

    @Autowired
    private TemplateDAO templateDAO;

    @Override
    @Cacheable(CacheNames.TEMPLATE)
    public List<Template> getAll() {
        return this.templateDAO.findAll();
    }

    @Override
    @Cacheable(CacheNames.TEMPLATE)
    public String getTemplate(final String name, final Locale locale) {
        return this.loadTemplate(name, locale).getTemplateBody();
    }

    @Override
    public DateTime getTemplateLastModifiedDate(final String name) {
        return this.loadTemplate(name, Locale.US).getChangedDate();
    }

    private Template loadTemplate(final String name, final Locale locale) {
        Template template = this.templateDAO.getTemplate(name, locale.getCountry(), locale.getLanguage());
        if (template == null) {
            template = new Template();
            template.setTemplateBody("Template does not exists!");
        }
        return template;
    }
}
