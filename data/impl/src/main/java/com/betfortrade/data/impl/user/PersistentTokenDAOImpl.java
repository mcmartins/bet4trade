/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.impl.user;

import com.betfortrade.data.api.dao.user.PersistentLoginDAO;
import com.betfortrade.data.api.dao.user.PersistentTokenDAO;
import com.betfortrade.data.api.entities.user.PersistentLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;

import java.util.Date;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class PersistentTokenDAOImpl implements PersistentTokenDAO {

    @Autowired
    private PersistentLoginDAO persistentLoginDAO;

    @Override
    public void insertToken(final PersistentRememberMeToken token) {
        final PersistentLogin persistentLogin = new PersistentLogin();
        persistentLogin.setId(token.getSeries());
        persistentLogin.setToken(token.getTokenValue());
        persistentLogin.setUsername(token.getUsername());
        persistentLogin.setLastUsed(token.getDate());
        this.persistentLoginDAO.save(persistentLogin);
    }

    @Override
    public void updateToken(final String series, final String tokenValue, final Date lastUsed) {
        final PersistentLogin persistentLogin = this.persistentLoginDAO.findOne(series);
        if (persistentLogin != null) {
            persistentLogin.setToken(tokenValue);
            persistentLogin.setLastUsed(lastUsed);
            this.persistentLoginDAO.save(persistentLogin);
        }
    }

    @Override
    public void deleteToken(final String username) {
        final PersistentLogin persistentLogin = this.persistentLoginDAO.findByUsername(username);
        if (persistentLogin != null) {
            this.persistentLoginDAO.delete(persistentLogin);
        }
    }

    @Override
    public PersistentRememberMeToken getTokenForSeries(final String seriesId) {
        final PersistentLogin persistentLogin = this.persistentLoginDAO.findOne(seriesId);
        return persistentLogin == null ? null :
                new PersistentRememberMeToken(persistentLogin.getUsername(), persistentLogin.getId(),
                        persistentLogin.getToken(), persistentLogin.getLastUsed());
    }

}
