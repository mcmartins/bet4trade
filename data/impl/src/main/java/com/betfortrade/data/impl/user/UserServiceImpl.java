/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.impl.user;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.betfortrade.core.util.ApplicationURL;
import com.betfortrade.core.util.SessionUtils;
import com.betfortrade.data.api.dao.general.CountryDAO;
import com.betfortrade.data.api.dao.general.LanguageDAO;
import com.betfortrade.data.api.dao.user.PasswordRecoveryDAO;
import com.betfortrade.data.api.dao.user.RoleDAO;
import com.betfortrade.data.api.dao.user.SubscriptionDAO;
import com.betfortrade.data.api.dao.user.UserDAO;
import com.betfortrade.data.api.entities.general.Country;
import com.betfortrade.data.api.entities.general.Language;
import com.betfortrade.data.api.entities.user.PasswordRecovery;
import com.betfortrade.data.api.entities.user.Person;
import com.betfortrade.data.api.entities.user.Profile;
import com.betfortrade.data.api.entities.user.Subscription;
import com.betfortrade.data.api.entities.user.User;
import com.betfortrade.data.api.service.user.UserService;
import com.betfortrade.messaging.api.email.EmailSender;
import com.betfortrade.messaging.api.template.Templates;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class UserServiceImpl implements UserService {

    private static final Logger LOG = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private SubscriptionDAO subscriptionDAO;

    @Autowired
    private RoleDAO roleDAO;

    @Autowired
    private PasswordRecoveryDAO passwordRecoveryDAO;

    @Autowired
    private LanguageDAO languageDAO;

    @Autowired
    private CountryDAO countryDAO;

    @Autowired
    private EmailSender<SimpleMailMessage> emailService;

    @Autowired
    private SimpleMailMessage mailMessage;

    @Override
    public List<User> getAll() {
        return this.userDAO.findAll();
    }

    @Override
    public User getUserByEmail(final String email) {
        return this.userDAO.findUserByEmail(email);
    }

    @Override
    public void saveUser(final String firstName, final String lastName, final String preferredName,
            final Date birthDate, final String email, final String phone, final String username,
            final String password, final String address1, final String address2, final String city,
            final String zipCode, final String country, final String language) {

        final User existingUser = this.userDAO.findUserByEmail(email);

        if (existingUser != null) {
            throw new RuntimeException("OOPS! What are you trying to do!");
        }

        final User user = new User();
        user.setEmail(email);
        user.setUsername(username);
        user.setPassword(this.passwordEncoder.encode(password));

        final Person person = new Person();
        person.setFirstName(firstName);
        person.setLastName(lastName);
        person.setPreferredName(preferredName);
        person.setAddress1(address1);
        person.setAddress2(address2);
        person.setBirthDate(new DateTime(birthDate));
        person.setCity(city);
        person.setPostalCode(zipCode);

        final Country countryObj = this.countryDAO.getCountryByISO(country);
        person.setCountry(countryObj);
        person.setPhoneNumber(phone);

        user.setPerson(person);
        person.setUser(user);

        final Profile profile = new Profile();
        Language languageObj = this.languageDAO.getLanguageByLocale(country, language);
        if (languageObj == null) {
            languageObj = this.languageDAO.getLanguageByLocale(Locale.UK.getCountry(), Locale.UK.getLanguage());
        }
        profile.setLanguage(languageObj);
        user.setProfile(profile);
        profile.setUser(user);

        user.getRoles().add(this.roleDAO.findRoleByName(com.betfortrade.core.authorization.Role.ROLE_USER.name()));

        final Subscription subscription = new Subscription();
        subscription.setSubscriptionValid(false);
        subscription.setLastSubscription(true);
        subscription.setSubscriptionToken(this.getNewToken());
        subscription.setUser(user);
        user.getSubscriptions().add(subscription);

        //save user
        this.userDAO.save(user);

        //notify user
        this.mailMessage.setTo(email);
        this.mailMessage.setSubject("Registration Email");
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("name", preferredName);
        params.put("link", SessionUtils.getHostName()
                + ApplicationURL.APPLICATION_REGISTRATION_VALIDATION.getUrl(subscription.getSubscriptionToken(), user.getEmail()));
        this.emailService.send(this.mailMessage, Templates.REGISTRATION, params);
    }

    @Override
    public boolean validateUser(final String email, final String validationToken) {
        boolean valid = true;
        final Subscription subscription = this.subscriptionDAO.findSubscriptionByEmail(email);
        if (subscription != null && !subscription.isSubscriptionValid()) {
            if (subscription.getSubscriptionToken().equals(validationToken)) {
                final DateMidnight dateTime = new DateMidnight();
                subscription.setSubscriptionStart(dateTime.toDateTime());
                subscription.setSubscriptionEnd(dateTime.toDateTime().plusMonths(1));
                subscription.setSubscriptionValid(true);
                this.subscriptionDAO.save(subscription);
            } else {
                throw new RuntimeException("OOPS! What are you trying to do!?");
            }
        } else {
            throw new RuntimeException("OOPS! What are you trying to do!?");
        }
        return valid;
    }

    @Override
    public String resetPassword(final String email) {
        String token;
        //check if already exists an active request
        PasswordRecovery passwordRecovery = this.passwordRecoveryDAO.findPasswordRecoveryByEmail(email);
        //if not, lets create one
        if (passwordRecovery == null) {
            final User user = this.userDAO.findUserByEmail(email);
            if (user != null) {
                token = this.getNewToken();
                passwordRecovery = new PasswordRecovery();
                passwordRecovery.setPasswordRecoveryToken(token);
                passwordRecovery.setUser(user);
                user.getPasswordRecoveries().add(passwordRecovery);

                //save it
                this.userDAO.save(user);

                //notify user
                this.mailMessage.setTo(email);
                this.mailMessage.setSubject("Registration Email");
                final Map<String, Object> params = new HashMap<String, Object>();
                params.put("name", user.getPerson().getPreferredName());
                params.put("link", SessionUtils.getHostName()
                        + ApplicationURL.APPLICATION_PASSWORD_RECOVERY.getUrl(token, user.getEmail()));
                this.emailService.send(this.mailMessage, Templates.RECOVERY_PASSWORD, params);
            } else {
                throw new RuntimeException("OOPS! What are you trying to do!? The user does not exist!");
            }
        } else {
            throw new RuntimeException("OOPS! What are you trying to do!? There is already a password recovery for this account!");
        }
        return token;
    }

    @Override
    public void resetPassword(final String email, final String token, final String newPassword) {
        //check if already exists an active request
        PasswordRecovery passwordRecovery = this.passwordRecoveryDAO.findPasswordRecoveryByEmail(email);
        if (passwordRecovery != null) {
            if (passwordRecovery.getPasswordRecoveryToken().equals(token)) {
                final User user = this.userDAO.findUserByEmail(email);
                //remove request
                user.getPasswordRecoveries().remove(passwordRecovery);
                //set password
                user.setPassword(this.passwordEncoder.encode(newPassword));
                //save it
                this.userDAO.save(user);
                //save password recovery
                passwordRecovery.setPasswordRecoveryValidated(true);
                this.passwordRecoveryDAO.save(passwordRecovery);
            } else {
                throw new RuntimeException("OOPS! What are you trying to do!? The token provided is not valid!");
            }
        } else {
            throw new RuntimeException("OOPS! What are you trying to do!? There is no password recovery for the current user!");
        }
    }

    @Override
    public void doSubscriptionMaintenance() {
        // expired subscriptions
        List<Subscription> subscriptions = this.subscriptionDAO.findSubscriptionExpiringIn(new Date());
        for (final Subscription subscription : subscriptions) {
            subscription.setSubscriptionValid(false);
            subscription.setNotifiedSubscriptionExpired(true);
            this.subscriptionDAO.save(subscription);

            //notify user
            this.mailMessage.setTo(subscription.getUser().getEmail());
            this.mailMessage.setSubject("Subscription Expired");
            final Map<String, Object> params = new HashMap<String, Object>();
            params.put("name", subscription.getUser().getPerson().getPreferredName());
            params.put("link", SessionUtils.getHostName()
                    + ApplicationURL.APPLICATION_SUBSCRIPTION_RENEWAL.getUrl(subscription.getUser().getEmail()));
            this.emailService.send(this.mailMessage, Templates.SUBSCRIPTION_EXPIRED, params);

            LOG.info("Maintenance for Subscription " + subscription.getUser().getId() + ", User " + subscription.getUser().getEmail());
        }
        // about to expire subscriptions
        subscriptions = this.subscriptionDAO.findSubscriptionExpiringIn(new DateTime().plusDays(15).toDate());
        for (final Subscription subscription : subscriptions) {
            subscription.setNotifiedSubscriptionExpiring(true);
            this.subscriptionDAO.save(subscription);

            //notify user
            this.mailMessage.setTo(subscription.getUser().getEmail());
            this.mailMessage.setSubject("Subscription Expiring");
            final Map<String, Object> params = new HashMap<String, Object>();
            params.put("name", subscription.getUser().getPerson().getPreferredName());
            params.put("link", SessionUtils.getHostName()
                    + ApplicationURL.APPLICATION_SUBSCRIPTION_RENEWAL.getUrl(subscription.getUser().getEmail()));
            this.emailService.send(this.mailMessage, Templates.SUBSCRIPTION_EXPIRING, params);

            LOG.info("Maintenance for Subscription " + subscription.getUser().getId() + ", User " + subscription.getUser().getEmail());
        }
    }

    @Override
    public void doPasswordRecoveryMaintenance() {
        // expired password recoveries
        final List<PasswordRecovery> passwordRecoveries = this.passwordRecoveryDAO.findAllPasswordRecoveryExpired(new Date());
        for (final PasswordRecovery passwordRecovery : passwordRecoveries) {
            passwordRecovery.setPasswordRecoveryExpired(true);
            this.passwordRecoveryDAO.save(passwordRecovery);

            LOG.info("Maintenance for Subscription " + passwordRecovery.getUser().getId() + ", User " + passwordRecovery.getUser().getEmail());
        }
    }

    @Override
    public Locale getUserLanguage(final String email) {
        final Language language = this.userDAO.getLanguageFromProfile(email);
        Locale locale = Locale.US;
        if (language != null) {
            locale = new Locale(language.getId().getCountryPK().getCountryISO(), language.getId().getLanguage());
        }
        return locale;
    }

    @Override
    public boolean isSubscriptionValid(final String email) {
        boolean valid = false;
        final Subscription subscription = this.subscriptionDAO.findSubscriptionByEmail(email);
        if (subscription != null) {
            valid = subscription.isSubscriptionValid();
        }
        return valid;
    }

    @Override
    public String getNewToken() {
        final String key = UUID.randomUUID().toString();
        return Joiner.on("").join(Iterables.transform(Splitter.on('-').split(key),
                new Function<String, String>() {
                    @Override
                    public String apply(final String from) {
                        final Random r = new Random();
                        final int x = r.nextInt(from.length());
                        return from.substring(0, from.length()) + from.substring(x).toUpperCase();
                    }
                }
        ));
    }
}
