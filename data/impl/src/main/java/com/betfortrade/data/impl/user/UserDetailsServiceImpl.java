/*
 * Copyright (c) Manuel Martins, All Rights Reserved.
 * (www.bet4trade.com)
 *
 * This software is the proprietary information of BetForTrade.
 * Use is subject to license terms.
 *
 */
package com.betfortrade.data.impl.user;

import com.betfortrade.data.api.entities.user.Role;
import com.betfortrade.data.api.service.user.UserDetailsService;
import com.betfortrade.data.api.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

/**
 * <description>.
 *
 * @author Manuel Martins
 */
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(final String username)
            throws UsernameNotFoundException, DataAccessException {
        com.betfortrade.data.api.entities.user.User user = this.userService.getUserByEmail(username);
        final List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        if (user != null) {
            for (final Role role : user.getRoles()) {
                authorities.add(new SimpleGrantedAuthority(role.getName()));
            }
        } else {
            user = new com.betfortrade.data.api.entities.user.User();
        }
        return new User(user.getUsername(), user.getPassword(), authorities);
    }
}
