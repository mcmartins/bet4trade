INSERT INTO Role (description, name, changedBy, changedDate, createdBy, creationDate) VALUES ('Administrator Role','ROLE_ADMINISTRATOR','system',NOW(),'system',NOW());
INSERT INTO Role (description, name, changedBy, changedDate, createdBy, creationDate) VALUES ('Super User Role','ROLE_SUPER_USER','system',NOW(),'system',NOW());
INSERT INTO Role (description, name, changedBy, changedDate, createdBy, creationDate) VALUES ('User Role','ROLE_USER','system',NOW(),'system',NOW());

