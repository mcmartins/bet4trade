INSERT INTO Language (countryISO, language, prettyName, changedBy, changedDate, createdBy, creationDate) VALUES ('GB','en','English (GB)','system',NOW(),'system',NOW());
INSERT INTO Language (countryISO, language, prettyName, changedBy, changedDate, createdBy, creationDate) VALUES ('US','en','English (US)','system',NOW(),'system',NOW());
INSERT INTO Language (countryISO, language, prettyName, changedBy, changedDate, createdBy, creationDate) VALUES ('FR','fr','French (FR)','system',NOW(),'system',NOW());
INSERT INTO Language (countryISO, language, prettyName, changedBy, changedDate, createdBy, creationDate) VALUES ('IT','it','Italian (IT)','system',NOW(),'system',NOW());
INSERT INTO Language (countryISO, language, prettyName, changedBy, changedDate, createdBy, creationDate) VALUES ('BR','pt','Portuguese (BR)','system',NOW(),'system',NOW());
INSERT INTO Language (countryISO, language, prettyName, changedBy, changedDate, createdBy, creationDate) VALUES ('PT','pt','Portuguese (PT)','system',NOW(),'system',NOW());
